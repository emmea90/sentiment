function [ model ] = createModel_simInj_bal(urm, param)
%[model] = createModel_simInj_bal(urm, param)
%   param.knntot: numero totale di vicini per item (content+collab)    
%   param.percentage: parameter used for determine the percentage of content
%                   similarities with respect to collaborative similarities.
%   param.contentInfluence: IMPORTANCE OF CONTENT with respect to COLLABORATIVE. Used for determining the number of pseudo-ratings to add
%   param.reservedPercentage: (COLLAB) percentage of knn slots assigned proportionally to "(1-param.contentInfluence=" parameter. 
%               The remaining knn slots depend on the number of ratings the 
%               item has and the value of "param.percentage" and "param.contentInfluence"
%   param.minNumberRatings: minimum number of ratings (per item) for fully trusting collaborative model
%
%   param.normalizeByMax: divide every element of the itemitem matrix by
%               its maximum. Default = 0
%   param.normalizeRowsByMax divide every rows of the itemitem matrix by
%               the maximum value of the row. Default = 0
%
%   param.collaborative.collab_param: struttura da passare a createModel_cosineIIknn
%   param.collaborative.createModel: 'm_collab=createModel_cosineIIknn(urm, param.collaborative.collab_param)'
%   param.collaborative.model: alternatively to .createModel, this
%       parameter directly contains the collaborative model
%   param.collaborative.itemitemMatrix: 'm_collab.II'
%   param.collaborative.threshold
%
%   param.content.content_param: struttura da passare a createModel_lsaCosine
%   param.content.createModel: 'm_content=createModel_lsaCosine(urm, param.content.content_param)'
%   param.content.model: alternatively to .createModel, this
%       parameter directly contains the content model
%   param.content.itemitemMatrix: 'm_content.dnorm'' * m_content.dnorm'
%   param.content.threshold
%
%   param.ratingcount = number of ratings per item
%
%   model     (compatibile con NNCosNgbr II)
%        .II = matrice di similarità item-item ibrida

%% CONSTANTS
%
% when #ratings=param.minNumberRatings, the weight of collaborative
% similarities is equals to MIN_NUM_RATING_WEIGHT
MIN_NUM_RATING_WEIGHT = 0.9; 

% when content influence is greater or equal to MAX_CONTENT_INFLUENCE, it
% is practivally considered equals to 1
MAX_CONTENT_INFLUENCE = 0.95;
%

%%
    if (nargin<2)
        param=struct();
    end
    
    if ~isfield(param,'knntot')
        param.knntot=200;
    end
    
    if ~isfield(param,'percentage')
        param.percentage=0.7;
    end    
    if ~isfield(param,'contentInfluence')
        param.contentInfluence=0.8;
    end        
    if ~isfield(param,'reservedPercentage')
        param.reservedPercentage=0.25;
    end       
    if ~isfield(param,'collaborative')
        param.collaborative=struct();
    end
    if ~isfield(param,'content')
        param.content=struct();
    end   
    if ~isfield(param.collaborative,'threshold')
        param.collaborative.threshold=-Inf;
    end
    if ~isfield(param.content,'threshold')
        param.content.threshold=-Inf;
    end    
    if ~isfield(param,'normalizeByMax')
        param.normalizeByMax=0;
    end
    if ~isfield(param,'normalizeRowsByMax')
        param.normalizeRowsByMax=0;
    end

    if ~isfield(param,'minNumberRatings')
        param.minNumberRatings=300;
    end    
    
    if ~isfield(param.collaborative,'collab_param')
        param.collaborative.collab_param=struct();
        param.collaborative.collab_param.knn=2*param.knntot;
    end
    if ~isfield(param.collaborative,'createModel') && ~isfield(param.collaborative,'model')
        param.collaborative.createModel='m_collab=createModel_cosineIIknn(urm, param.collaborative.collab_param)';
    end    
    if ~isfield(param.collaborative,'itemitemMatrix')
        param.collaborative.itemitemMatrix='m_collab.II';
    end    

    if ~isfield(param.content,'content_param')
        param.content.content_param=struct();
        warning('CREATEMODELSIMINJBAL:ICMMISSING','missing Item-Content Matrix: this might be required by ');
    end
    if ~isfield(param.content,'createModel') && ~isfield(param.content,'model')
        param.content.createModel='m_content=createModel_lsaCosine(urm, param.content.content_param)';
    end    
    if ~isfield(param.content,'itemitemMatrix')
        param.content.itemitemMatrix='m_content.dnorm'' * m_content.dnorm';
    end       
    
    h=waitbar(0,'computing collaborative model..');
    
    %m_collab = createModel_cosineIIknn(urm, param.collab_param); IIcollab= m_collab.II;
    if ~isfield(param.collaborative,'model')
        eval([param.collaborative.createModel,';']);
    else
        m_collab=param.collaborative.model;
    end 
    waitbar(0.125,h);
    eval(['IIcollab=',param.collaborative.itemitemMatrix,';']);
    waitbar(0.25,h,'computing content model..');
    
    %m_content = createModel_lsaCosine(urm, param.content_param); IIcont =m_content.dnorm' * m_content.dnorm;
    if ~isfield(param.content,'model')
        eval([param.content.createModel,';']);
    else
        m_content=param.content.model;
    end
    waitbar(0.375,h);
    eval(['IIcont=',param.content.itemitemMatrix,';']);
    waitbar(0.5,h,'computing hybrid thresholds..');
    
    if ~isfield(param,'ratingcount')
        nrat = sum(urm~=0);
    else
        nrat = param.ratingcount;
    end
    knntot = param.knntot;

    IIcollab(IIcollab<param.collaborative.threshold)=0;
    IIcont(IIcont<param.content.threshold)=0;
    
    IIcollab = filterkNN(IIcollab,knntot);
    IIcont = filterkNN(IIcont,knntot);   
    
    %IIcont=IIcont-diag(diag(IIcont));
    %IIcollab=IIcollab-diag(diag(IIcollab));
    
    if (param.normalizeByMax)
        IIcont=IIcont / (max(max(IIcont))/(param.contentInfluence+0.1));
        IIcollab=IIcollab / (max(max(IIcollab))/(1-param.contentInfluence+0.1));
    end
    
    if (param.normalizeRowsByMax)
        h2=waitbar(0,'normalizing collaborative matrix..');
        IIcollab=(diag(1./(max(IIcollab,[],2)+0.5)))*IIcollab*(1-param.contentInfluence);
        waitbar(0.5,h2,'normalizing content matrix..');
        IIcont=(diag(1./(max(IIcont,[],2)+0.5)))*IIcont*(param.contentInfluence);
        close(h2);
    end
    
    %{
    IIcont=IIcont / (prctile(nonzeros(IIcont),95)/(param.contentInfluence));
    IIcollab=IIcollab / (prctile(nonzeros(IIcollab),95)/(1-param.contentInfluence));
    %}
%    IIcont(IIcont<param.thres) = 0;
    %IIcont(IIcollab~=0) = 0;
    %IIcont = IIcont+(1-param.percentage)*IIcollab;

    [m, n] = size(IIcont);
    model.II = spalloc(m,n, n*knntot); 
    
    % fisso punti per interpolazione 
    if param.contentInfluence>0 && param.contentInfluence<MAX_CONTENT_INFLUENCE && quantile(nrat, 0.95)>param.minNumberRatings
        x = full([0 quantile(nrat, param.percentage) quantile(nrat, 0.95)]);
        y = full([0 (1-param.contentInfluence) 1]);
        
        if (x(1)==x(2))
            x=full([0 param.percentage*max(nrat) max(nrat)]);
            y=full([0 0 (1-param.contentInfluence)]);
%            x=[0 max(nrat)];
%            y=[0 (1-param.contentInfluence)];
        end
    else
        if quantile(nrat,0.95)<=param.minNumberRatings
            nrat = [nrat param.minNumberRatings];
        end            
        if param.contentInfluence==0
            x=[0 max(nrat)];
            y=[1 1];
        elseif param.contentInfluence>=MAX_CONTENT_INFLUENCE
            x=[0 max(nrat)];
            y=[0 0];
        else
            x=full([0 param.percentage*max(nrat) max(nrat)]);
            y=full([0 0 (1-param.contentInfluence)]);
        end
    end
    
    knntmp_collab=ceil(knntot*(param.reservedPercentage)); %knntmp_collab=ceil(knntot*(1-param.reservedPercentage));
    collabSim_shrink = ((1-MIN_NUM_RATING_WEIGHT)/MIN_NUM_RATING_WEIGHT) * param.minNumberRatings; % shrinking factor for collaborative similarites
    
    waitbar(0.75,h,'computing hybrid model..');
    try
        %timeHandle = tic;
        for i=1:n
            reservedPercentage=param.reservedPercentage;
            if (nrat(i)<param.minNumberRatings)
                reservedPercentage=param.reservedPercentage*nrat(i)/param.minNumberRatings;
            end
            
            if (nrat(i)<max(x))
                %knn_collab = ceil(interp1(x,y, full(nrat(i)))*knntot);
                knn_collab = ceil(interp1(x,y, full(nrat(i))) * knntmp_collab);
            else
                knn_collab = (knntot*(reservedPercentage)); % knn_collab = (knntot*(1-reservedPercentage));
            end
           knn_collab = knn_collab + ceil((knntot*reservedPercentage)*(1-param.contentInfluence));
           knn_cont = knntot - knn_collab;
           [v idxcol] = sort(-IIcollab(:,i)); 
           %model.II(idxcol(1:knn_collab), i) = IIcollab(idxcol(1:knn_collab), i); 
           model.II(idxcol(1:knn_collab), i) = (nrat(i)/(nrat(i)+collabSim_shrink)) * IIcollab(idxcol(1:knn_collab), i); 
           [v idxcon] = sort(-IIcont(:,i));
           
           indexesCol=idxcol(1:knn_collab);
           indexesCon=idxcon(1:knn_cont);
           
           indexesCommon=intersect(indexesCol, indexesCon);
           indexesOnlycontent=setdiff(indexesCon,indexesCol);
           model.II(indexesCommon, i) = model.II(indexesCommon, i) + (param.contentInfluence)*IIcont(indexesCommon, i);
           model.II(indexesOnlycontent, i) = IIcont(indexesOnlycontent, i);
           %model.II(idxcon(1:knn_cont), i) = model.II(idxcon(1:knn_cont), i) + (param.percentage)*IIcont(idxcon(1:knn_cont), i);
           if mod(i, 500) == 0
                %timeHandle = displayRemainingTime(i, n, timeHandle);
                waitbar(0.75+0.25*i/n,h);
           end
        end
    catch e
        close(h);
        rethrow(e);
    end
    close(h);
end

