function [ model ] = createModel_simInj_knn(urm, param)
%[model] = createModel_simInj_knn(urm, param)
%   param.collab_param: struttura da passare a createModel_cosineIIknn
%                     .knn: numero di vicini collab.
%   param.content_param: struttura da passare a createModel_lsaCosine
%                      .knn: numero di vicini content
%   model     (compatibile con NNCosNgbr II)
%        .II = matrice di similarità item-item ibrida


    m_collab = createModel_cosineIIknn(urm, param.collab_param);
    m_content = createModel_lsaCosine(urm, param.content_param);
    
    IIcont = m_content.dnorm' * m_content.dnorm;
    % priorità al collaborativo
    IIcont(m_collab.II~=0) = 0;

    model = m_collab;
    
    for i=1:size(IIcont,2)
       [v idx] = sort(-IIcont(:,i));
       model.II(idx(1:param.content_param.knn), i) = ...
           IIcont(idx(1:param.content_param.knn), i);
    end
    % (knn content)+(knn collab) valori di similarità in ogni colonna
end

