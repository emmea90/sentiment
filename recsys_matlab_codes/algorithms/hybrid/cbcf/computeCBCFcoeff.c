#include "cs_mex.h"
#include <stdio.h>

#define log 1

#define H_FACTOR 50.0
#define MAX 2.0

void mexFunction(int nargout,
		 mxArray *pargout[],
		 int nargin,
		 const mxArray *pargin[])
{
	cs_dl Pmatrix, Umatrix, *P, *U;
	double h_factor, max;

	UF_long i, p, pp, Pm, Pn, Pnzmax, Pnz, *Pp, *Pi;
	UF_long j, q, qq, Um, Un, Unzmax, Unz, *Up, *Ui;
	UF_long r, rr, ni, nj, corated_ij;

	double mi, mj, hm_ij, sg_ij, hw_ij, phw_ij, sw_i;
	double *Px, *Ux;

	cs *PhwMatrix;
	
	mxArray *SwMatrix;
	double *sw_values;

	if (nargout > 2 || nargin > 4 || nargin < 2) {
		mexErrMsgTxt("Usage: [Phw [,Sw]] = computeCBCFcoeff(pearsonCoeff, URM [,h_factor [,max]])");
		return;
	}

	/* defaults */
	h_factor = H_FACTOR;
	max = MAX;

	/* arguments override */
	if (nargin >= 3) {
		h_factor = mxGetScalar(pargin[2]);
		if (nargin == 4) {
			max = mxGetScalar(pargin[3]);
		}
	}
	
	P = cs_dl_mex_get_sparse (&Pmatrix, 0, 1, pargin[0]);
	U = cs_dl_mex_get_sparse (&Umatrix, 0, 1, pargin[1]);
	

	if (NULL == P || NULL == U) {
		if (log) {
			printf("(null)\n");
		}
		return;
	}

	Pm = P->m;          	Um = U->m;
	Pn = P->n;              Un = U->n;
	Pp = P->p;              Up = U->p;
	Pi = P->i;              Ui = U->i;
	Px = P->x;              Ux = U->x;
	Pnzmax = P->nzmax;      Unzmax = U->nzmax;
	Pnz = P->nz;            Unz = U->nz;

	PhwMatrix = cs_spalloc(Pn, Pm, Pnz, 1, 1);


	if (log) {
		fprintf(stdout, "P: mxn = %dx%d", Pm, Pn);
		fprintf(stdout, "U: mxn = %dx%d", Um, Un);
	}

	fprintf(stdout, "Begin computation of Phw\n");
	fflush(stdout);

	for (i = 0; i < Pn; i++) {
		/* scan matrix P */
		p = Pp[i];
		pp = Pp[i+1];
		
		for (j = p; j < pp; j++) {
			/* correlation between user i and j exists 
			 *   P(i,j) != 0
			 */

			/* retrieve user i and j from the urm */
			q = Up[Pi[j]];    /* user j indexes */
			qq = Up[Pi[j]+1];

			r = Up[i];    /* user i indexes */  
			rr = Up[i+1];

			/* compute weighing factors for CBCF */
			ni = 0;     nj = 0;
			mi = 0;     mj = 0;
			hm_ij = 0;  
			hw_ij = 0;
			sg_ij = 0;
			corated_ij = 0;
			
			while (qq && rr && q<qq && r<rr) {
				
				if (Ui[r] == Ui[q]) {  
					/* CO-RATED */
					corated_ij++;
					r++;
					q++;
					ni++;
					nj++;
				}
				else if (Ui[r] > Ui[q]) {
					/* rated by i and not by j */
					q++;
					nj++;
				}
				else {
					/* rated by j and not by i */
					r++;
					ni++;
				}

			}
			
			/* count remaining ratings */
			while (q<qq) {
				q++;
				nj++;
			}
			while (p<pp) {
				p++;
				ni++;
			}

			/* compute parameters */			
			mi = (ni < h_factor)? (ni / h_factor) : 1.0;
			mj = (nj < h_factor)? (nj / h_factor) : 1.0;
			
			hm_ij = 2*mi*mj / (double)(mi + mj);
			sg_ij = (corated_ij < h_factor)? (corated_ij / h_factor) : 1.0;

			hw_ij = hm_ij + sg_ij;
			phw_ij = Px[j] * hw_ij;

			cs_entry(PhwMatrix, i, Pi[j], phw_ij);
		
			
		}

		if (i%1000 == 0) {
			fprintf(stdout, "%ld of %ld done \n", i, Pn);
			fflush(stdout);
		}
	}

	PhwMatrix = cs_compress(PhwMatrix);
	pargout[0] = cs_dl_mex_put_sparse(&PhwMatrix);

	fprintf(stdout, "Phw coefficients done.\n");
	fflush(stdout);

	if (nargout == 2) {
		/* compute self weights (sw_i)*/

		SwMatrix = mxCreateDoubleMatrix(Un, 1, mxREAL);
		sw_values = mxGetPr(SwMatrix);

		for (i = 0; i < Un; i++) {
			p = Up[i];
			pp = Up[i+1];
			ni = 0;
			while (pp && p < pp) {
				ni++;
				p++;
			}
			sw_i = (ni < h_factor)? (ni / h_factor)*max : max;
			sw_values[i] = sw_i;
		}

		pargout[1] = SwMatrix;
		fprintf(stdout, "Sw coefficients done.\n");
		fflush(stdout);

	}
}
