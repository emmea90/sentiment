function [ model] = createModel_cbcf( URM, DTM, param )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

    addpath('./bayes/');
    %addpath('./pearson/');
    addpath(genpath('/home/roby/sparseMatrices/CXSparse/MATLAB/'));    

    % defaults
    max = 2;
    h_factor = 50;
    k = 30; % k nearest neighbors
    
    if nargin > 2
        if isfield(param,'max')
            max = param.max;
        end
        if isfield(param, 'h_factor')
            h_factor = param.h_factor;
        end
        if isfield(param, 'k')
            k = param.k;
        end
    end
    
    
    param_p.k = k; 
    model.k = k; % save k in model
    
    
    disp('Generating bayesian model');
    model.bayes = createModel_bayes(URM,DTM);
    
    model.R = URM;
    
    disp('Generating Pearson model');
    model.pearson = createModel_pearson(URM, param_p);
    %colvectors = URM';
    %model.pearson.P = computePearsonSimilarity(colvectors, 30, 0);    

    nr = sum(URM~=0, 2);
    n = (nr>=h_factor) + nr.*(nr<h_factor)/h_factor;
    
    n_users = size(URM,1);
    
    
    disp('Generating Hybrid CBCF model');
    
    sw = zeros(n_users,1);
    
    Phw = spalloc(n_users, n_users, n_users*k);
    P = model.pearson.P;

    parfor a=1:n_users
        ra = URM(a,:);
        hw = zeros(n_users,1);
        for u=a:n_users
             
            co_rated = nnz(ra.*(URM(u,:)));
            %co_rated = length(intersect(nnz_a, URM(u,:)~=0));
            
            if co_rated > h_factor
                sg = 1;
            else
                sg = co_rated / h_factor;
            end
            
            hw(u) = 2*n(a)*n(u)/(n(a)+n(u)) + sg;
            
        end
        
        Phw(a,:) = P(a,:).*hw';
        
        if nr(a) < h_factor
            sw(a) = nr(a)/h_factor * max;
        else
            sw(a) = max;
        end
        
        if mod(a/n_users*100,10) == 0
            disp(a/n_users*100);
        end

    end
    
    model.sw = sw;
    model.Phw = Phw;

end

