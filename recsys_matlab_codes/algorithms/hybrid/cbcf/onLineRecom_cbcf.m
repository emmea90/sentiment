function [ recomList ] = onLineRecom_cbcf( userProfile, model, param )
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here

%     if nargin>2
%         if(isfield(param, 'k'))
%             k = param.k; 
%         else
%             k = 30;
%         end
%     else
%         k=30;
%     end

%    addpath('/home/airoldi/matlab/Engineering/cbcf/bayes/');
    
    
    user = param.userToTest;
    max_rating = model.max_rating;
    
    k = model.k;
    n_users = size(model.R,1);
    n_items = size(model.R,2); 

    bayesParam.max_rating = max_rating;
    bayesParam.userToTest = user;
   
%       Pu = model.Phw(user,:)+model.Phw(:,user)';       
%	Pu = full(Pu);
	Pu = full(model.Phw(:,user));
%        Pu(1,user) = 0;
        
        [waste,  u, knn] = find(Pu);
        
        a = user;
        
        Ca = onLineRecom_bayes_user_mex(model.R(a,:), model.bayes, bayesParam)';
        Va = Ca.*(model.R(a,:)==0) + model.R(a,:);
	sw_a = model.sw(a);       
%        C = zeros(nnz(u), n_items);
%        V = zeros(nnz(u), n_items);
	C=zeros(n_items, nnz(u));
	V=zeros(n_items, nnz(u));
        
        
        R = model.R';
        bayes = model.bayes;
        for ii=1:nnz(u)
	    bayesParam.userToTest = u(ii);
            C(:,ii) = onLineRecom_bayes_user_mex(model.R(u(ii),:), bayes, bayesParam)';
            V(:,ii) = C(:,ii).*(R(:,u(ii))==0) + R(:,u(ii));
        end 
       
	num = sw_a*(Ca-mean(Va)) + ...
		sum( diag(knn) * (V' - mean(V',2)*ones(1,size(V',2)) ));
	den = sum(knn) + sw_a;

       
        p_user = mean(Va) + (num ./ den);
%        Rma = sum(model.R(a,:))./nnz(model.R(a,:));
%        p_user = Rma + (num ./ den);
 
       %end
    
       p_user(p_user>max_rating) = max_rating;
       p_user(p_user<1) = 1;
    recomList = p_user;
    
     if (nargin>=3)
        if(isfield(param,'postProcessingFunction'))
            recomList=feval(param.postProcessingFunction,recomList,param);
        end
    end
   
end


