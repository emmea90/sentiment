function [ model ] = createModel_cbcf_mex(URM, param )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%addpath('./bayes/');
    %addpath('./pearson/');
    addpath(genpath('/home/roby/sparseMatrices/CXSparse/MATLAB/'));
    %   addpath(genpath('/home/airoldi/matlab/Engineering/C/'));

    % defaults
    max = 2;
    h_factor = 50;
    k = 30; % k nearest neighbors
    model.max_rating = param.max_rating;
    DTM = param.dtm;
    model.dtm = DTM;
    if nargin > 2
        if isfield(param,'max')
            max = param.max;
        end
        if isfield(param, 'h_factor')
            h_factor = param.h_factor;
        end
        if isfield(param, 'k')
            k = param.k;
        end
    end

    param_p.k = k; 
    model.k = k; % save k in model
    
    
    disp('Generating bayesian model');
    model.bayes = createModel_bayes(URM, param);
    
    model.R = URM;
    
    disp('Generating Pearson model');
    colvectors = URM';
    model.pearson.P = computePearsonSimilarity(colvectors, 30, 0);
    
    [Phw, Sw] = cbcf(model.pearson.P, colvectors, h_factor, max);
    
    model.Phw = Phw;
    model.sw = Sw;

end

