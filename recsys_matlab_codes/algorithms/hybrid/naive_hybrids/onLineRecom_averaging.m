function [ recomList ] = onLineRecom_averaging( userProfile, model, params )
%ONLINERECOM_AVERAGING Summary of this function goes here
%   Detailed explanation goes here

    if (nargin>=3)
        p = params;   
        if(isfield(params,'postProcessingFunction'))
           
            p = rmfield(p, 'postProcessingFunction');
        end
    end
   
     
    
    onLineRecom_contentFcn = params.onLineRecom_contentFcn;
    onLineRecom_collabFcn = params.onLineRecom_collabFcn;
    
    rl_content = feval(onLineRecom_contentFcn, userProfile, model.content, p);
    rl_collab = feval(onLineRecom_collabFcn, userProfile, model.collab, p);
    
    recomList = (rl_content + rl_collab) / 2;    
    
    
     if (nargin>=3)
        if(isfield(params,'postProcessingFunction'))
            recomList=feval(params.postProcessingFunction,recomList,params);
        end
     end
    
end

