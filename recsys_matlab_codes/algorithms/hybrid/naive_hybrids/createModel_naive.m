function [ model ] = createModel_naive( urm, params )
%CREATEMODEL_AVERAGING Summary of this function goes here
%   Detailed explanation goes here

    model.max_rating = params.max_rating;
    
    createModel_contentFcn = params.createModel_contentFcn;
    createModel_collabFcn = params.createModel_collabFcn;
    
    model.content = feval (createModel_contentFcn, urm, params);
    model.collab = feval(createModel_collabFcn, urm, params);
    
end

