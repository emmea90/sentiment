function [ recomList ] = onLineRecom_max( userProfile, model, params)
%ONLINERECOM_MAX return recomList composed of max(collab, content)
%   [ recomList ] = onLineRecom_max(userProfile, model, params)
%   userProfile: vector with user ratings
%   model: a naive_hybrid model
%   params: structure
%         .onLineRecom_contentFcn: function handler to function used
%                                 for content-based recommendations
%         .onLineRecom_collabFcn: function handler to function used
%                                for collaborative recommendations
%
%   params will be passed to those 2 functions, so it may contain
%   other fields.
% 
%   RETURN VALUE:
%   recomList is a vector which elements are the maximum between
%   the collaborative and the content-based recommendation
%


    param = params;
    if (nargin>=3)
        if(isfield(params,'postProcessingFunction'))
            param = rmfield(param, 'postProcessingFunction');
        end
     end

    onLineRecom_contentFcn = params.onLineRecom_contentFcn;
    onLineRecom_collabFcn = params.onLineRecom_collabFcn;
    
    rl_content = feval(onLineRecom_contentFcn, userProfile, model.content, param);
    rl_collab = feval(onLineRecom_collabFcn, userProfile, model.collab, param);
    
    rl_hybrid = rl_content;    
    idx = rl_collab>rl_content;
    rl_hybrid(idx) = rl_collab(idx);    
    
    recomList = rl_hybrid;
    
    if (nargin>=3)
        if(isfield(params,'postProcessingFunction'))
            recomList=feval(params.postProcessingFunction,recomList,params);
        end
     end

end

