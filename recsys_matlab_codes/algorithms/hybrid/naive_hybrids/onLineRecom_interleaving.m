function [ recomList ] = onLineRecom_interleaving( userProfile, model, params)
%ONLINERECOM_INTERLEAVING interleaves predictions from a 2 algorithms
%   Detailed explanation goes here

    param = params;
    if (nargin>=3)
        if(isfield(params,'postProcessingFunction'))
            param = rmfield(param, 'postProcessingFunction');
        end
     end

     param.postProcessingFunction = 0;

    onLineRecom_contentFcn = params.onLineRecom_contentFcn;
    onLineRecom_collabFcn = params.onLineRecom_collabFcn;
    
    rl_content = feval(onLineRecom_contentFcn, userProfile, model.content, param);
    rl_collab = feval(onLineRecom_collabFcn, userProfile, model.collab, param);

    done = zeros(size(rl_collab));
    recomList = zeros(size(rl_collab));
    n = length(rl_collab);
    i1 = 1;
    i2 = 1;
    [r1, idx1] = sort(rl_collab,'descend');
    [r2, idx2] = sort(rl_content, 'descend');
    d = 1;
    
    while d <= n; 
        if i1 <= n
            while (i1 < n && done(idx1(i1))~=0)
                i1 = i1 + 1;
            end
            recomList(idx1(i1)) = (n-d);
            done(idx1(i1)) = 1;
            i1 = i1 + 1;
            d = d + 1;
        end
        
        if i2 <= n && d <= n
            while (i2 < n && done(idx2(i2))~=0)
                i2 = i2 + 1;
            end
            recomList(idx2(i2)) = n-d;
            done(idx2(i2)) = 1;
            i2 = i2 + 1;
            d = d + 1;
        end
        
        
    end
    
    
    if (nargin>=3)
        if(isfield(params,'postProcessingFunction'))
             if(strcmp(class(params.postProcessingFunction),'function_handle'))
                recomList=feval(params.postProcessingFunction,recomList,params);    
             end
        end
     end

end
