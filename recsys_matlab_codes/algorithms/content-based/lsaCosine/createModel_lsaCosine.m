function [model] = createModel_lsaCosine (URM,param)
% URM = matrix with user-ratings
% param.ls = latent size
% [param.itemModel] = matrice projItem=s*v' (non necessariamente normalizzata)
% [param.icm] = Item-content matrix

    if isfield(param,'itemModel')
        d=param.itemModel;
        if isfield(param,'ls')
            ls=param.ls;
            d=d(1:ls,:);
        end        
    else
        if isfield(param,'icm')
            if isfield(param,'ls')
                ls=param.ls;
            else
                ls=300;
            end                  
    	    if (exist('svdmex')==3)
	        [u,s,v]=svdmex(param.icm, ls);
		s = diag(s);
            else
		display(['svdmex not found.. try to add sparseMatrices directory to matlab path. Built-in svd will be used this time']);
	        [u,s,v]=svds(param.icm,ls);
	    end
            d = s*v';
        else
            warning('lsa: not enough parameters..');
        end
    end
    
    if isfield(param,'shrinking')
        shrinking = param.shrinking;
    else
        shrinking = 0;
    end
    display(['shrinking=',num2str(shrinking)]);
    
    if shrinking>0
        dnorm = normalizeShrinkageColsMatrix(d,shrinking); %shrinked cosine
    else
        dnorm = normalizeColsMatrix(d); % cosine
    end
    model.dnorm = dnorm; 
end
