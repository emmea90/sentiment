#include <omp.h>
#include <stdio.h>
#include <math.h>
#include <mex.h>

#define CHUNKSIZE 1

#define DEBUG 0

/* 
   pargin[0]: m_estimate for class i
   pargin[1]: document-term matrix
   pargin[2]: probability of class i

   pargout[0]: ratings
*/

double *
bayesJoint(double *mx,
	   const mxArray *dtm,
	   double p_class,
	   double *res)

{
	mwSize n_items, i, p, pp; 
	double log_prob, base_log_prob;
	long *jc, *ir;

	n_items = mxGetN(dtm);

	jc = mxGetJc(dtm);
	ir = mxGetIr(dtm);

	base_log_prob = log(p_class);

        /* predict rating for each item */
	for (i = 0; i < n_items; i++) {
		p = jc[i];
		pp = jc[i+1];
			
		log_prob = base_log_prob;
	
		while (pp && p<pp) {

			log_prob += log(mx[ir[p]]);
			p++;

		}

		res[i] = log_prob;
	}
 
}


/*
  prhs[0]: userprofile
  prhs[1]: dtm
  prhs[2]: n_class
  prhs[3]: dict_size
  prhs[4]: p_class

 */

void 
mexFunction(int nlhs,
	    mxArray *plhs[],
	    int nrhs,
	    const mxArray *prhs[])
{

	mwSize n_items, n_terms, c, i, j, p, pp, q, qq, term, item, max_idx; 
	long *dtm_jc, *dtm_ir, *up_jc, *up_ir;
	double *dtm_x, *up_x;
	double *itemjoint;
	double *n, *m_estimate, *p_class, *recomList;

	double mr, ds, val;
	int n_class;

	if (nrhs != 5) {
		mexErrMsgTxt("Five inputs required.");
	}

	if (nlhs > 1) {
		mexErrMsgTxt("Too many output arguments.");
	}

	if (!mxIsSparse(prhs[0])) {
		mexErrMsgTxt("userProfile must be sparse.");
	}

	if (!mxIsSparse(prhs[1])) {
		mexErrMsgTxt("dtm must be sparse.");
	}

	if (!mxIsScalar(prhs[2])){
		mexErrMsgTxt("n_class must be scalar.");
	}

	if (!mxIsScalar(prhs[3])){
		mexErrMsgTxt("dict_size must be scalar.");
	}

	if (mxIsSparse(prhs[4])) {
		mexErrMsgTxt("p_class must be full.");
	}

	if (mxGetN(prhs[0]) != 1) {
		mexErrMsgTxt("userProfile must be a column array.");
	}

	n_items = mxGetN(prhs[1]);
	n_terms = mxGetM(prhs[1]);

	if (mxGetM(prhs[0]) != n_items) {
		mexErrMsgTxt("Wrong userProfile size.");
	}

	n_class = mxGetScalar(prhs[2]);

	if (mxGetM(prhs[4]) != 1) {
		mexErrMsgTxt("p_class must be a row array.");
	}

	if (mxGetN(prhs[4]) != n_class) {
		mexErrMsgTxt("Wrong p_class size.");
	}

	
	ds =mxGetScalar(prhs[3]);

	dtm_jc = mxGetJc(prhs[1]);
	dtm_ir = mxGetIr(prhs[1]);
	dtm_x = mxGetPr(prhs[1]);
   
	up_jc = mxGetJc(prhs[0]);
	up_ir = mxGetIr(prhs[0]);
	up_x = mxGetPr(prhs[0]);

	p_class = mxGetPr(prhs[4]);
	
	itemjoint = mxCalloc(n_class*n_items, sizeof(double));

	n = mxMalloc(sizeof(double)*n_terms);
	m_estimate = mxMalloc(sizeof(double)*n_terms);
	recomList = mxMalloc(n_items*sizeof(double));

	/* initialize arrays */
	for (i = 0; i < n_terms; i++) {
		n[i] = ds;
	}

	/* calculate values for n (the entire denominator) */
	p = up_jc[0];
	pp = up_jc[1];

	while (pp && p<pp) {
		
		/* up_ir[p] is a seen item */
		item = up_ir[p];

		q = dtm_jc[item];
		qq = dtm_jc[item+1];

		while (qq && q<qq) {
			
			term = dtm_ir[q];
			n[term]++;
			q++;
		}
		p++;
		
	}

	/* for each class */
	for (c = 0; c < n_class; c++) {

		/* re-initialize m_estimate */
		for (i = 0; i < n_terms; i++) {
			m_estimate[i] = 1;
		}

		/* compute the m_estimate for each term */
		p = up_jc[0];
		pp = up_jc[1];

		while (pp && p<pp) {
		
			/* up_ir[p] is a seen item */
			item = up_ir[p];
			
			if (up_x[p] == (c+1)) {

				q = dtm_jc[item];
				qq = dtm_jc[item+1];

				while (qq && q<qq) {
						
					term = dtm_ir[q];
					m_estimate[term] += 1.0;
					q++;
				}
			}
			p++;
		
		}

		/* update m-estimate */
		for (i = 0; i < n_terms; i++) {
			m_estimate[i] /= n[i];
		}

		bayesJoint(m_estimate,prhs[1],p_class[c],&itemjoint[c*n_items]);

	}


	/* itemjoint [n_items x n_class] */
	/* find max idx */
	for (i = 0; i < n_items; i++) {
		max_idx = 0;
		for (c = 1; c < n_class; c++) {
			if (itemjoint[i+c*n_items] > itemjoint[i + n_items*max_idx]) {
				max_idx = c;
			}
		}
		recomList[i] = max_idx +1;
	}
 
	plhs[0] = mxCreateDoubleMatrix(n_items, 1, mxREAL);
	mxSetPr(plhs[0],recomList);

	mxFree(n);
	mxFree(m_estimate);
}
