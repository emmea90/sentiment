#include <omp.h>
#include <stdio.h>
#include <math.h>
#include <mex.h>

#define CHUNKSIZE 1

#define DEBUG 0


double *
bayesJoint(double *mx,
	   const mxArray *dtm,
	   double p_class,
	   mwSize i,
	   double *res)

{
	mwSize n_items, p, pp; 
	double log_prob, base_log_prob;
	long *jc, *ir;

	n_items = mxGetN(dtm);

	jc = mxGetJc(dtm);
	ir = mxGetIr(dtm);

	base_log_prob = log(p_class);

        /* predict rating for item i */
	
	p = jc[i];
	pp = jc[i+1];
			
	log_prob = base_log_prob;

	while (pp && p<pp) {
		log_prob += log(mx[ir[p]]);
		p++;

	}

	res[0] = log_prob;   
 
}


/*
  prhs[0]: user
  prhs[1]: dtm
  prhs[2]: n_class
  prhs[3]: dict_size
  prhs[4]: p_class
  prhs[5]: item
  prh6[6]: urm

 */

double 
rating_ui(mwSize user,
	  const mxArray *dtm,
	  mwSize n_class,
	  double ds,
	  const mxArray *pc,
	  mwSize it,
	  const mxArray *urm)
{

	mwSize n_items, n_terms, c, i, p, pp, q, qq, term, item, idx_max; 
	long *dtm_jc, *dtm_ir, *urm_jc, *urm_ir;
	double *dtm_x, *urm_x;
	double *itemjoint;
	double *n, *m_estimate, *p_class, *tmp;

	double mr, val;

	n_items = mxGetN(dtm);
	n_terms = mxGetM(dtm);

	dtm_jc = mxGetJc(dtm);
	dtm_ir = mxGetIr(dtm);
	dtm_x = mxGetPr(dtm);
   
	urm_jc = mxGetJc(urm);
	urm_ir = mxGetIr(urm);
	urm_x = mxGetPr(urm);

	p_class = mxGetPr(pc);
	
	itemjoint = calloc(n_class, sizeof(double));

	n = malloc(sizeof(double)*n_terms);
	m_estimate = malloc(sizeof(double)*n_terms);

	/* initialize arrays */
	for (i = 0; i < n_terms; i++) {
		n[i] = ds;
	}

	/* calculate values for n (the entire denominator) */
	p = urm_jc[user];
	pp = urm_jc[user + 1];

	while (pp && p<pp) {
		
		/* urm_ir[p] is a seen item */
		item = urm_ir[p];

		q = dtm_jc[item];
		qq = dtm_jc[item+1];

		while (qq && q<qq) {
			
			term = dtm_ir[q];
			n[term]++;
			q++;
		}
		p++;
		
	}

	/* for each class */
	for (c = 0; c < n_class; c++) {


		/* re-initialize m_estimate */
		for (i = 0; i < n_terms; i++) {
			m_estimate[i] = 1;
		}

		/* compute the m_estimate for each term */
		p = urm_jc[user];
		pp = urm_jc[user + 1];

		while (pp && p<pp) {
		
			/* urm_ir[p] is a seen item */
			item = urm_ir[p];
			
			if (urm_x[p] == (c+1)) {

				q = dtm_jc[item];
				qq = dtm_jc[item+1];

				while (qq && q<qq) {
						
					term = dtm_ir[q];
					m_estimate[term] += 1.0;
					q++;
				}
			}
			p++;
		
		}

		/* update m-estimate */
		for (i = 0; i < n_terms; i++) {
			m_estimate[i] /= n[i];
		}

		bayesJoint(m_estimate,dtm,p_class[user*n_class+c],it,&itemjoint[c]);
		
	}
 
	idx_max = 0;
	for (i = 1; i < n_class; i++) {
		if(itemjoint[i] > itemjoint[idx_max]) { 
			idx_max = i;
		}
	}

	free(n);
	free(m_estimate);
	free(itemjoint);

	return (double)(idx_max + 1);
}


/*
  0: item
  1: urm
  2: dtm
  3: n_class
  4: p_class
  5: dict_size

 */

void 
mexFunction(int nlhs,
	    mxArray *plhs[],
	    int nrhs,
	    const mxArray *prhs[])
{

	mwSize item, n_class, n_users, i;
	double ds, *ratings;

	if (!mxIsScalar(prhs[3])) {
		mexErrMsgTxt("n_class must be scalar");
	}
	
	if (!mxIsScalar(prhs[5])) {
		mexErrMsgTxt("dict_size must me scalar");
	}

	if (!mxIsSparse(prhs[1])) {
		mexErrMsgTxt("urm must be sparse");
	}

	if (!mxIsSparse(prhs[2])) {
		mexErrMsgTxt("dtm must be sparse");
	}

	if (mxIsSparse(prhs[4])) {
		mexErrMsgTxt("p_class must be full");
	}

	n_class = mxGetScalar(prhs[3]);

	if (n_class <=1) {
		mexErrMsgTxt("n_class must be >1");
	}

	ds = mxGetScalar(prhs[5]);

	if (ds <= 1) {
		mexErrMsgTxt("dict_size must be >1");
	}

	item = mxGetScalar(prhs[0])-1;
	if (item < 0) {
		mexErrMsgTxt("item must be a positive index");
	}
	
	n_users = mxGetN(prhs[1]);

	if (n_users != mxGetN(prhs[4])) {
		mexErrMsgTxt("urm and p_class must have the same number of columns");
	}

	if (mxGetM(prhs[4]) != n_class) {
		mexErrMsgTxt("p_class must have n_class rows");
	}

	if (mxGetM(prhs[1]) != mxGetN(prhs[2])) {
		mexErrMsgTxt("The number of columns in dtm must be equal to the number of rows in urm");
	}

	ratings = mxMalloc(sizeof(double)*n_users);

	for (i = 0; i < n_users; i++) {
		ratings[i] = rating_ui(i, prhs[2],n_class,ds,prhs[4],item,prhs[1]);
		
	}

	plhs[0] = mxCreateDoubleMatrix(n_users,1, mxREAL);
	mxSetPr(plhs[0], ratings);

}
