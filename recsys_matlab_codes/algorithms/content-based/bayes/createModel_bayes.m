function [model] = createModel_bayes (urm, param)
% URM = matrix with user-ratings
% DTP = document-term matrix

    dtm = param.dtm;
    model.dtm = dtm;
    model.urm = urm;
    
    % defaults
    max_rating = 5;
    
    % parameter override
    if nargin > 2
        if isfield(param, 'max_rating')
            max_rating = param.max_rating;
        end
    end

    n_users = size(urm,1);
    
    p_class = zeros(n_users, max_rating);

    timeHandle=tic;
    display(' - Generating Naive Bayes model - ');
    %for u=1:n_users
    nnz_ratings = sum(urm~=0,2);
   
    for ii=1:max_rating
        %p_class(u,ii) = nnz(urm(u,:)==ii)./nnz_ratings;
        % m-estimate
        p_class(:,ii) = (sum(urm==ii,2)+1)./(nnz_ratings+max_rating);
       
        
        timeHandle = displayRemainingTime(ii, max_rating, timeHandle);
    end    

    %fprintf(1,'done %d of %d\n',u, n_users);
    %    end

    model.p_class = p_class;
    model.dict_size = size(dtm, 1);
    
    
    display([' - Naive bayes model crated in ',num2str(toc(timeHandle)),' sec - ']);
end
