function [recomList] = onLineRecom_bayes_user_mex (userProfile, model ,param)
%userProfile = vector with ratings of a single user
%model = model created with createModel function 
%param
%param.postProcessingFunction = handle of post-processing function (e.g., business-rules)

    % defaults
    max_rating = 5;
    
    % parameter overrides
    if nargin > 2
        if (isfield(param, 'max_rating'))
            max_rating = param.max_rating;
        end
    end
    
    
    user = param.userToTest;
    dtm = model.dtm;
    urm = model.urm;
    dict_size = model.dict_size;
    p_class = model.p_class;
    
    recomList = recomBayes_user_mex(userProfile',dtm, max_rating, dict_size, p_class(user,:));
    
    if (nargin>=3)
        if(isfield(param,'postProcessingFunction'))
            if(strcmp(class(param.postProcessingFunction),'function_handle'))
                recomList=feval(param.postProcessingFunction, ...
                                recomList,param);
            end
        end
    end 
end
