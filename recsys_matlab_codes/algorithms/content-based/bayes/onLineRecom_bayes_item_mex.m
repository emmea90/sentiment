function [recomList] = onLineRecom_bayes_item_mex (item, model ,param)
%item = item index in urm
%model = model created with createModel function 
%param
%param.postProcessingFunction = handle of post-processing function (e.g., business-rules)

    % defaults
    max_rating = 5;
    
    % parameter overrides
    if nargin > 2
        if (isfield(param, 'max_rating'))
            max_rating = param.max_rating;
        end
    end
    
    
   
    dtm = model.dtm;
    urm = model.urm;
    dict_size = model.dict_size;
    p_class = model.p_class;
    
    recomList = recomBayes_item_mex(item, urm',dtm, max_rating, p_class', dict_size);
    
    if (nargin>=3)
        if(isfield(param,'postProcessingFunction'))
            recomList=feval(param.postProcessingFunction,recomList,param);
        end
    end
    
    
    
end
