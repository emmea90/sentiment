function [model] = createModel_pureSVD (URM, modelParam)
% URM = matrix with user-ratings
% modelParam = modelParam.ls specifies the latenSize

ls=50;
if (exist('modelParam')~=0)
    if (isstruct(modelParam))
        if (isfield(modelParam,'ls')) 
            ls = modelParam.ls;
        end
    end
end
    
    if (exist('svdmex')==3)
        [u, s, v] = svdmex(URM, ls);
    else
        display(['svdmex not found.. try to add sparseMatrices directory to matlab path. Built-in svd will be used this time']);
        [u,s,v]=svds(URM,ls);
    end
    model.vt=v';     
end