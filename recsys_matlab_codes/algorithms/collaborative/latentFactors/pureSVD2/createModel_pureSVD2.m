function [model] = createModel_pureSVD2 (URM, modelParam)
% URM = matrix with user-ratings
% modelParam = modelParam.ls specifies the latenSize
% modelParam.trainedUnbiased = true|false. If this parameter is true,
%   ratings are first unbiased (with global mean, user bias and item bias)
%   before proceeding with SVD
% modelParam.loadModel = load from file previous model
% modelParam.saveModel = save to file the computed model

ls=50;
trainedUnbiased = false;

if nargin<2
    modelParam=struct();
end

if (isfield(modelParam,'loadModel'))
    if fileexist(modelParam.loadModel)
        Mtmp = load(modelParam.loadModel);
        if isfield(Mtmp,'model')
            model = Mtmp.model;
            clear Mtmp;
            return;
        end
    end
end

if (isfield(modelParam,'ls')) 
    ls = modelParam.ls;
end
if isfield(modelParam,'trainedUnbiased')
    trainedUnbiased = modelParam.trainedUnbiased;
end

    
if trainedUnbiased 
    if (isfield(modelParam,'lambdaI'))
        lambdaI = param.lambdaI;
    else 
        lambdaI=25;
    end
    if (isfield(modelParam,'lambdaU'))
        lambdaU = modelParam.lambdaU;
    else 
        lambdaU=10;
    end      
    
    mu = full(sum(sum(URM,1),2));
    mu = mu/nnz(URM);
    
    tic

    display(' - computing biases - ');
    nnzI=zeros(1,size(URM,2));
    nnzU=zeros(1,size(URM,1));

    display(['length nnzI=',num2str(length(nnzI))]);

    timeHandleBiases=tic;
    for i=1:length(nnzI)
        nnzI(i)=nnz(URM(:,i)); 
        if (mod(i,5000)==0),
            displayRemainingTime(i, length(nnzI),timeHandleBiases);           
        end       
    end
    bi = (sum(URM,1) - mu*(nnzI)) ./ (lambdaI + nnzI);

    URMt=URM';
    for i=1:length(nnzU)
        nnzU(i)=nnz(URMt(:,i)); 
        if (mod(i,50000)==0),
            displayRemainingTime(i, length(nnzU),timeHandleBiases);           
        end       
    end   
    
    timeHandleBiases=tic;
    splitSize=100;
    splitNum=ceil(size(URM,1)/splitSize);
    tosub=zeros(1,size(URM,1));
    for i=1:splitNum
        maxRowNum=min([i*splitSize,size(URM,1)]);
        rowIndexes=splitSize*(i-1)+1:maxRowNum;

        %tosub(rowIndexes)=sum(spones(URMt(:,rowIndexes)'.*(ones(length(rowIndexes),1)*bi)),2);
        tosub(rowIndexes)=sum(spones(URMt(:,rowIndexes)').*(ones(length(rowIndexes),1)*bi),2);


        if (mod(i,500)==0),
            displayRemainingTime(i, splitNum,timeHandleBiases);           
        end          
    end
    bu = (sum(URM,2)' - mu*nnzU - tosub) ./ (lambdaU + nnzU);    
    clear URMt;      
    
    model.mu=mu;
    model.bu_precomputed=bu;
    model.bi_precomputed=bi;
    
    toc
    tic
    display(' - computing URM-biases - ');
    
    % removing biases
    splitSize=50;
    splitNum=ceil(size(URM,2)/splitSize);
    for i=1:splitNum
        maxColNum=min([i*splitSize,size(URM,2)]);
        colIndexes=splitSize*(i-1)+1:maxColNum;
        tmpURM=URM(:,colIndexes);
        tmpModifiedURM = tmpURM - ones(size(URM,1),1)*model.bi_precomputed(colIndexes) - model.bu_precomputed'*ones(1,length(colIndexes)) - model.mu;
        [tmpIndexes]=find(tmpURM==0);
        tmpModifiedURM(tmpIndexes)=0;
        URM(:,colIndexes)=tmpModifiedURM;
        
        if (mod(i,50)==0),
            displayRemainingTime(i, splitNum,timeHandleBiases);           
        end          
    end    
    clear tmpIndexes tmpModifiedURM tmpURM colIndexes
    toc
end

    
    if (exist('svdmex')==3)
        [u, s, v] = svdmex(URM, ls);
    else
        display(['svdmex not found.. try to add sparseMatrices directory to matlab path. Built-in svd will be used this time']);
        [u,s,v]=svds(URM,ls);
    end
    model.vt=v';   
    
if (isfield(modelParam,'saveModel'))
	save(modelParam.saveModel,'model');
end    
    
end