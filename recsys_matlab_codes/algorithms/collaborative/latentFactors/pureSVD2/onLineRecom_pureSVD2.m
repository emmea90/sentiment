function [recomList] = onLineRecom_pureSVD2 (userProfile, model,param)
%userProfile = vector with ratings of a single user
%model = model created with createModel function 
%param.ls = [optional] latent size
%param.postProcessingFunction = handle of post-processing function (e.g.,business-rules)

    vt = model.vt;
    if (nargin>=3)
        if (isfield(param,'ls'))
            vt=vt(1:param.ls,:); 
        end
    end
   
    if isfield(model,'mu') & isfield(model,'bu_precomputed') & isfield(model,'bi_precomputed')
        mu = model.mu;
        bu = model.bu_precomputed;
        bi = model.bi_precomputed;
        
        if isfield(param,'userToTest')
            bu=bu(param.userToTest);
        else
            bu=sum(userProfile)/nnz(userProfile);
        end
        ratedItems=find(userProfile);
        unbiasedUserProfile=userProfile;
        unbiasedUserProfile(ratedItems)=unbiasedUserProfile(ratedItems)-mu-bu-bi(ratedItems);
        recomList=(mu+bu+bi)+ unbiasedUserProfile*vt'*vt;
    else
        recomList=userProfile*vt'*vt;
    end
    
    if (nargin>=3)
        if(isfield(param,'postProcessingFunction'))
            recomList=feval(param.postProcessingFunction,recomList,param);
        end
    end
end