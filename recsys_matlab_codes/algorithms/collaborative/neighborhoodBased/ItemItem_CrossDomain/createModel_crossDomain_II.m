function [model] = createModel_crossDomain_II (URM,param)
% URM = matrix with user-ratings
% param.baseCreateModel=@createModel_cosineIIknn
% param.baseDR = 'II'
% param.outDR = 'II'
%
% param.createAdditionalLinks = @transitiveClosure / @katzClosure (no default)
% param.closureNumStep = 2 (only for transitiveClosure)
% param.beta = 0.8 (only for katzClosure)
%
% param.overweight = 1;
% param.areasToEnhance = {struct('items1',setdiff(Ia,Ib),'items2',setdiff(Ib,Ia)), 
%                         struct('items1',setdiff(Ib,Ia),'items2',setdiff(Ia,Ib))};
%
% param.hybrid = false;
% param.areasToEnhance = {struct('items1',setdiff(Ia,Ib),'items2',setdiff(Ib,Ia)), 
%                         struct('items1',setdiff(Ib,Ia),'items2',setdiff(Ia,Ib))};
%
% param.simThresholdBase = -inf;
%
% param.saveModel = 'filepath'
% param.loadModel = 'filepath'
% param.loadEnhancedModel = 'filepath'
% param.saveBaseModel = 'filepath'
% param.loadBaseModel = 'filepath'


    if (exist('param')==0)
        param = struct();
    end
    if (~isfield(param,'baseDR'))
        param.baseDR = 'II';
    end
    if (~isfield(param,'simThresholdBase')) 
        param.simThresholdBase = -inf;
    end
    if (~isfield(param,'outDR'))
        param.outDR = 'II';
    end    
    if (isfield(param,'loadModel'))
        try
            load(param.loadModel);
        catch e
            warning (['error while loading ', param.loadModel]);
        end
    end
    if (exist('model')==1)
        if (isfield(model,param.outDR))
            display(['crossDomain - model correctly loaded from ', param.loadModel]);
            return;
        end
    end
    if (isfield(param,'loadEnhancedModel'))
        try
            load(param.loadEnhancedModel);
        catch e
            warning (['error while loading ', param.loadEnhancedModel]);
        end
    end   
    if (exist('model')==1)
        dr= getfield(model,param.outDR);
        display(['crossDomain - enhanced model correctly loaded from ', param.loadEnhancedModel]);
    else

        display(['crossDomain - model is being computed']);

        if (~isfield(param,'baseCreateModel'))
            param.baseCreateModel=@createModel_cosineIIknn;
        end

        fileName=[];
        if (isfield(param,'loadBaseModel'))
            fileName = param.loadBaseModel;
        else
            if (isfield(param,'loadModel'))
                if (exist([param.loadModel,'_baseModel']))
                    fileName = [param.loadModel,'_baseModel'];
                elseif (exist([param.loadModel,'_baseModel','.mat']))
                    fileName = [param.loadModel,'_baseModel','.mat'];
                end
            end
        end
        if ~isempty(fileName)
            try
                load(fileName);
                display(['loaded baseModel from ',fileName]);
            catch 
                display(['error while loading ',fileName]);
            end
        end

        fileName=[];
        if (isfield(param,'saveBaseModel'))
            fileName = param.saveBaseModel;
        elseif (isfield(param,'saveModel'))
            try
                [pathstr, onlyname, onlyext]=fileparts(param.saveModel);
                fileName = [pathstr,filesep,onlyname,'_baseModel',onlyext];
                clear pathstr onlyname onlyext;
            catch
                fileName = [param.saveModel,'_baseModel'];
            end
        end    

        if exist('baseModel')~=1
            baseModel = feval(param.baseCreateModel, URM, param); 
            if (~isempty(fileName))
                save(fileName, 'baseModel');
            end
        end
        
        model = baseModel;
        baseDR = sparse(eval(['baseModel.', param.baseDR]));

        dr=baseDR;
        
        if ~isinf(param.simThresholdBase)
            display('filtering threshold');
            dr(find(dr<param.simThresholdBase))=0;
        end

        if (isfield(param,'createAdditionalLinks'))
            if (~isfield(param,'closureNumStep'))
                param.closureNumStep = 2;
            end
            if (~isfield(param,'beta'))
                param.beta = 0.8;
            end        
            display('cross domain - additional links started');
            dr = feval(param.createAdditionalLinks, dr, param);
        end
    end
    
    if (~isfield(param,'overweight'))
        param.overweight = 1;
    end

    if (param.overweight~=1)
        if ~isfield(param,'areasToEnhance')
            error('overweight requires areasToEnhance');
        end
        display('cross domain - overweight started');
        for i=1:length(param.areasToEnhance)
            areaToEnhance = param.areasToEnhance{i};
            dr(areaToEnhance.items1, areaToEnhance.items2) = dr(areaToEnhance.items1, areaToEnhance.items2) * param.overweight;
        end
    end
    
    if isfield(param,'hybrid')
        if (param.hybrid==true)
            if ~isfield(param,'areasToEnhance')
                error('hybrid requires areasToEnhance');
            end
            display('cross domain - hybrid started');
            drTmp = dr;
            dr = baseDR;
            for i=1:length(param.areasToEnhance)
                areaToEnhance = param.areasToEnhance{i};
                dr(areaToEnhance.items1, areaToEnhance.items2) = drTmp(areaToEnhance.items1, areaToEnhance.items2) * param.overweight;
            end
        end
    end

    eval(['model.', param.outDR,'=dr;']);
    
    if (isfield(param,'saveModel'))
        save(param.saveModel,'model');
        display(['crossdomain: model saved in ', param.saveModel]);
    end
end