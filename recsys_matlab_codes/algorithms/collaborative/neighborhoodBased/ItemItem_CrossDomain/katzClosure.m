function closedMatrix = katzClosure(baseSimilarityMatrix, param)
% function closedMatrix = katzClosure(baseSimilarityMatrix, param)
% param.beta

if (nargin>=2)
    if isfield(param,'nStep')
        if (nStep~=2) 
            warning('katzClosure does not support nStep~=2');
        end
    end
    if ~isfield(param,'beta')
        param.beta=0.8;
    end
else
    param.beta=0.8;
end

t1=tic;
disp('Katz closure: started');
    I = speye(size(baseSimilarityMatrix, 1));
    M = I - param.beta*baseSimilarityMatrix;
    if ~issparse(M)
        M=sparse(M);
    end
    score = sppinv(M) - I;
    score(score<10^(-3)) = 0;
    %closedMatrix = max(score, baseSimilarityMatrix);
    closedMatrix = score;
disp(['Katz closure: complete in time: ', num2str(round(toc(t1)*10)/10),' sec']);
end


function X = sppinv(A,varargin)
    %SPPINV   Pseudoinverse for sparse matrices.
    %   X = PINV(A) produces a matrix X of the same dimensions
    %   as A' so that A*X*A = A, X*A*X = X and A*X and X*A
    %   are Hermitian. The computation is based on SVD(A) and any
    %   singular values less than a tolerance are treated as zero.
    %   The default tolerance is MAX(SIZE(A)) * NORM(A) * EPS(class(A)).
    %
    %   PINV(A,TOL) uses the tolerance TOL instead of the default.
    %
    %   Class support for input A: 
    %      float: double, single
    %
    %   See also RANK.

    %   Copyright 1984-2004 The MathWorks, Inc. 
    %   $Revision: 5.12.4.2 $  $Date: 2004/12/06 16:35:27 $

    if isempty(A)     % quick return
      X = zeros(size(A'),class(A));  
      return  
    end

    [m,n] = size(A);

    if n > m
       X = sppinv(A',varargin{:})';
    else
       [U,S,V] = svds(A,200);
       if m > 1, s = diag(S);
          elseif m == 1, s = S(1);
          else s = 0;
       end
       if nargin == 2
          tol = varargin{1};
       else
          tol = max(m,n) * eps(max(s));
       end
       r = sum(s > tol);
       if (r == 0)
          X = zeros(size(A'),class(A));
       else
          s = diag(ones(r,1)./s(1:r));
          X = V(:,1:r)*s*U(:,1:r)';
       end
    end
end