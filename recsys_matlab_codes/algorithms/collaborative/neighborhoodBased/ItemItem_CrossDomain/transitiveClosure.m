function closedMatrix = transitiveClosure(baseSimilarityMatrix, param)
% function closedMatrix = transitiveClosure(baseSimilarityMatrix, param)
% param.nStep

nStep=2;
if (nargin>=2)
    if isfield(param,'nStep')
        nStep=param.nStep;
    end
end

t1=tic;
disp('Transitive closure: started');

     simStep = baseSimilarityMatrix;
     closedMatrix = baseSimilarityMatrix;
     for i = 1 : nStep - 1
         splitsize=200;
         fatto=false;
         while (~fatto & splitsize>1)
             try
                 if exist('cs_multiply')==3
                     display('trying cs_multiply mex file');
                     simStep=cs_multiply(baseSimilarityMatrix,simStep);
                     fatto = true;
                 else
                     display('cs_multiply mex file NOT FOUND: multiplication might be very slow');
                    simStep=baseSimilarityMatrix*simStep;
                    fatto = true;
                 end
             catch
                 try
                    simStep = sparseMatrixMultiplication(baseSimilarityMatrix,simStep,splitsize,splitsize*2);
                    fatto = true;
                 catch errme
                    display(errme.message);
                    display(['memory issues while running "sparseMatrixMultiplication.m".. trying splitsize=',num2str(splitsize)]); 
                    splitsize=floor(splitsize/2); 
                    if splitsize<=1 
                        error('crossdomain:sparsematrixmultiplicationMEMORY','error while computing transitive closure');
                    end
                 end
             end
         end
         closedMatrix = max(simStep, closedMatrix); % the union is performed by a max
     end

     % It is possible that the similarity matrix has some >1 values: we
     % divide all the values by the max one
     maxVal = max(max(closedMatrix));
     if maxVal > 1
         try 
            %closedMatrix = closedMatrix./maxVal;
         
            
            cols=size(closedMatrix,2);
            splitSizeCol=100;
            colsplits = ceil(cols/splitSizeCol);
            for j=1:colsplits
                maxNumOfCols=min([j*splitSizeCol,cols]);
                colsIndexes=splitSizeCol*(j-1)+1:maxNumOfCols;
                closedMatrix(:,colsIndexes) = spfun(@(x) x./maxVal, closedMatrix(:,colsIndexes));
            end
         catch me
            warning('memory problem while rescaling transitive-closure matrix.. '); 
            display(me.message);
         end
     end
     disp(['Transitive closure: complete in time: ', num2str(round(toc(t1)*10)/10),' sec']);
end