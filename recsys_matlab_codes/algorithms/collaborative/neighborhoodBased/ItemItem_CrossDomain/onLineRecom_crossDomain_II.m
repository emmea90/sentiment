function [recomList] = onLineRecom_crossDomain_II (userProfile, model,param)
%userProfile = vector with ratings of a single user
%model = model created with createModel function 
%param
%param.postProcessingFunction = handle of post-processing function (e.g., business-rules)
%param.onLineFunction = @onLineRecom_NNCosNgbr_II_knn

    onlinefunction = @onLineRecom_NNCosNgbr_II_knn;
    if (nargin>=3)
        if(isfield(param,'onLineFunction'))
            onlinefunction=param.onLineFunction;
        end
    end

    recomList = feval(onlinefunction,userProfile,model,param);
  
    if (nargin>=3)
        if(isfield(param,'postProcessingFunction'))
            recomList=feval(param.postProcessingFunction,recomList,param);
        end
    end
    
end