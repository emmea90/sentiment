function [recomList] = onLineRecom_cosineUUknn (userProfile, model,param)
%userProfile = vector with ratings of a single user
%model = model created with createModel function 
%param.knn = [optional] k-nearest neighbors
%param.postProcessingFunction = handle of post-processing function (e.g., business-rules)

    dr = model.UU;
    URM = param.URM;
    active = param.userToTest;
    
    % filtering KNN!
    if (nargin>=3)
        if (isfield(param,'knn'))
            knn=param.knn;
            if (knn<size(dr,2))
                for i=1:size(dr,2), dr(i,i)=0;  end   % annullo diagonale
                UU = sparse(size(dr,1),size(dr,2));     % creo matrice vuota (sparsa)
                for i=1:size(UU,2)
                   colItem = dr(:,i);
                   [r c] = sort(colItem,1,'descend');
                   itemToKeep = c(1:knn);
                   UU(itemToKeep,i) = dr(itemToKeep,i);
                end
                dr = UU;
            end
        end
    end
    
    recomList = dr(active, :) * URM;
    %recomList=userProfile*dr;
    
    if (nargin>=3)
        if(strcmp(class(param.postProcessingFunction),'function_handle'))
            recomList=feval(param.postProcessingFunction,recomList,param);
        end
    end
    
end