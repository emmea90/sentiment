function [model] = createModel_cosineUUknn (URM,param)
% URM = matrix with user-ratings
% param: 	empty --> esegue classico coseno
%                   [param.model] --> opzionale, contiene gi� la matrice dr
%                   e i bias
%					param.drCos --> opzionale, contiene gi� la matrice DR=URM'*URM
%					param.knn --> contiene il numero di K da considerare nella matrice DR
%                   param.memoryProblem --> boolean, se true impone l'utilizzo della
%                                           versione ottimizzata per matrici sparse grandi
%                   param.computeBaseline --> compute mu, bu and bi
%                        .lambdaI
%                        .lambdaU
%                   param.SimilarityShrinkage --> enable shrinking for
%                                                 COSINE similarity (shrinking factor is lambdaS)
%                        .lambdaS --> shrinking factor

model = feval(@createModel_cosineIIknn,URM',param);

model.UU = model.II;
model = rmfield(model,'II');

end