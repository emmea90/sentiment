function [model] = createModel_ricciRemoteAverage_CrossDomain (URM,param)
% [model] = createModel_ricciRemoteAverage_CrossDomain (URM,param)
% URM = matrix with user-ratings
% param: 	
%   param.Ua
%   param.Ia
%   param.Ub
%   param.Ib
%
%   param.createModel
%   param.param_a
%   param.param_b
%
%
% model:    
%   model.model_a
%   model.model_b

if nargin<2 
    help createModel_ricciRemoteAverage_CrossDomain
    return;
end

%URM_a = sparse(size(URM,1),size(URM,2));
%URM_a(param.Ua,param.Ia) = URM(param.Ua,param.Ia);
URM_a = copyMatrix(URM,param.Ua,param.Ia);
model.model_a = feval(param.createModel,URM_a,param.param_a);

clear URM_a;

%URM_b = sparse(size(URM,1),size(URM,2));
%URM_b(param.Ub,param.Ib) = URM(param.Ub,param.Ib);
URM_b = copyMatrix(URM,param.Ub,param.Ib);
model.model_b = feval(param.createModel,URM_b,param.param_b);
clear URM_b;

end

function [M2] = copyMatrix(M,righe,colonne)
    M2 = sparse(size(M,1),size(M,2));
    splitSize=200;
    splitNum=ceil(length(colonne)/splitSize);
    timeHandle = tic ;
    for j=1:splitNum
        maxNumOfCols=min([j*splitSize,length(colonne)]);
        colIndexes=splitSize*(j-1)+1:maxNumOfCols;
        M2(righe,colonne(colIndexes)) = M(righe,colonne(colIndexes));
        timeHandle=displayRemainingTime(maxNumOfCols, length(colonne),timeHandle);
    end
end