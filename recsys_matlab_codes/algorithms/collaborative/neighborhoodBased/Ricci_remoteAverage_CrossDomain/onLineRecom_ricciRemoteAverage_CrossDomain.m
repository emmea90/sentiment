function [recomList] = onLineRecom_ricciRemoteAverage_CrossDomain (userProfile, model,param)
%userProfile = vector with ratings of a single user
%model = model created with createModel function 
%param

    onlinefunction = @onLineRecom_NNCosNgbr_II_knn;
    if (nargin>=3)
        if(isfield(param,'onLineFunction'))
            onlinefunction=param.onLineFunction;
        end
    else
        param=struct();
    end

    recomList_a = feval(onlinefunction,userProfile,model.model_a,param);
    recomList_b = feval(onlinefunction,userProfile,model.model_b,param);
    
    recomList = ones(length(recomList_a),1)*-inf;
    
    items_a = find(recomList_a~=0 & ~isinf(recomList_a));
    items_b = find(recomList_b~=0 & ~isinf(recomList_b));
    commonItems = intersect(items_a,items_b);
    items_onlya=setdiff(items_a,commonItems);
    items_onlyb=setdiff(items_b,commonItems);
    
    recomList(items_onlya)=recomList_a(items_onlya);
    recomList(items_onlyb)=recomList_b(items_onlyb);
    recomList(commonItems)=(recomList_a(commonItems)+recomList_b(commonItems))./2;
    
  
    if (nargin>=3)
        if(isfield(param,'postProcessingFunction'))
            recomList=feval(param.postProcessingFunction,recomList,param);
        end
    end
    
end