function [model] = createModel_pearsonUUkoren (URM,param)
% URM = matrix with user-ratings
% param: 	param.knn --> K similar items
%           [param.lambdaS] -->  shrinking factor per s_{ij}
%           [param.Cdisabled] --> DISABLE C-compiled execution
%
%
% model:    model.lambdaS -->  shrinking factor USED per s_{ij}
%			model.knn --> Knn USED

    if nargin<2 
        param=struct();
    end
    if ~isfield(param,'knn')
        param.knn=round(size(urm,1)*0.1);
        display(['knn = ',num2str(param.knn)]);
    end
    if ~isfield(param,'lambdaS')
        param.lambdaS = 0;
    end

    model.UU = computePearsonSimilarity(URM', param.knn, param.lambdaS);
    model.urm = URM; 
    model.knn=param.knn;

end