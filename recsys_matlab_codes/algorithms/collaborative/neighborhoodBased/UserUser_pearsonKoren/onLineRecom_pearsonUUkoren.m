function [recomList] = onLineRecom_pearsonUUkoren (userProfile, model,param)
%userProfile = vector with ratings of a single user
%model = model created with createModel function 
%
%param.knn = [optional] k-nearest neighbors
%param.urm = URM
%param.postProcessingFunction = handle of post-processing function (e.g., business-rules)

   
    active = param.userToTest;
    try
        UU = model.UU;           
        if (isfield(model,'knn'))
            knnModel=model.knn;
        else
            knnModel=inf;
        end
    catch e
        error(' -- onLineRecom_pearsonIIkoren: missing fields in the model!');
    end
    
    % filtering KNN!
    if (nargin>=3)
        if (isfield(param,'knn'))
            knn=param.knn;
            if (knn<size(UU,2) && knn<knnModel)
                UU = filterKnn(UU,knn);
            end
        end
    end    
    
    Pu = UU(:,active);
    [idx, waste, knnsim] = find(Pu); %idx=indees of all users similar to target user
    den =  sum(abs(knnsim));
    if den == 0
      recomList =zeros(1,size(userProfile,2));
    else
        user_avg = (sum(userProfile)/nnz(userProfile))*ones(1,size(model.urm,2));
        other_avg = (sum(model.urm(idx,:),2)./sum(model.urm(idx,:)~=0,2))*ones(1,size(model.urm(idx,:),2));
        r_diff = model.urm(idx,:) - other_avg.*model.urm(idx,:)~=0;

        recomList = user_avg + (sum(diag(knnsim)*r_diff)) / den;
    end

    if (nargin>=3)
        if(isfield(param,'postProcessingFunction'))
            recomList=feval(param.postProcessingFunction,recomList,param);
        end
    end
    
end