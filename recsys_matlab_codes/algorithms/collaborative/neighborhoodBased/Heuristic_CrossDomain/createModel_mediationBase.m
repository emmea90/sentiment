% Antonio Tripodi
% Politecnico di Milano
% 2010, Dec. 8th - Dec. 11th

function [model] = createModel_mediationBase (URM,param)   
% INPUT:    URM = ratings matrix in the form user-item
%           param = a set of useful parameters. In this case the indication
%           if we have to work in an item-based or user-based environmente
%           and the last column/last row for each domain
% OUTPUT:   model = a similarity matrix for each domain
% It is exactly the same as "createModel_mediationBase"
    
    params.knn = 100;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   if we work in an user-based way     %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if param.userOrItem == 'u'
        lastCol = param.lastCol;
        numDomains = length(lastCol) + 1;
        domURM = cell(1, numDomains);
        domSim = cell(1, numDomains);
        for i = 1 : numDomains
            if i == 1
                domURM{i} = URM(:, 1:lastCol);
            else
                domURM{i} = URM(:, lastCol + 1 : end);
            end
            domSim{i} = createModel_cosineIIknnmod(domURM{i}', params);
        end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   if we work in an item-based way     %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
    else
        lastRow = param.lastRow;
        numDomains = length(lastRow) + 1;
        domURM = cell(1, numDomains);
        domSim = cell(1, numDomains);
        for i = 1 : numDomains
            if i == 1
                domURM{i} = URM(1:lastRow, :);
            else
                domURM{i} = URM(lastRow + 1 : end, :);
            end
            domSim{i} = createModel_cosineIIknnmod(domURM{i}, params);
        end
    end
    
    model.domSim = domSim;
end