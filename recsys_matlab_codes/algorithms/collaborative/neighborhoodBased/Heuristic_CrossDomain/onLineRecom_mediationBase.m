% Antonio Tripodi
% Politecnico di Milano
% 2010, Dec. 8th - Dec. 11th

function [recomList] = onLineRecom_mediationBase (userProfile, model,param)
% INPUT:    userProfile = the URM row related to the active user
%           model = the model created in the createModelFunction, in this
%           case a similarity matrix for each domain
%           param = a set of useful parameters. In this case the active
%           user, the minimum similarity threshold to consider users or
%           items as neighbors and the URM
% OUTPUT:   recomList = the similarity values of items to be recommended
% Each domain is seen as uncorrelated to each other: so each one creates
% its own predictions

    active = param.userToTest;
    simThreshold = param.simThreshold;
    URM = param.URM;
    domSim = model.domSim;
    numDomains = size(domSim, 2);
    domRec = zeros(numDomains, size(URM, 2));
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   if we work in an user-based way     %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    if param.userOrItem == 'u'
        for i = 1 : numDomains
            % We extract the neighbors (above simThreshold) 
            [r, neighbors] = find(domSim{i}(active, :) > simThreshold);
            % We extract items seen by neighbors
            simValues = domSim{i}(active, neighbors);
            % Now we create recommendations
            domRec(i, :) = simValues*URM(neighbors, :); 
        end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%   if we work in an item-based way     %%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    else
        seen = find(userProfile ~= 0);
        for i = 1 : numDomains
            neighbors = [];
            for j = 1 : length(seen)
                % We extract similar items to the ones seen by the user
                [r, c] = find(domSim{i}(seen(j), :) > simThreshold);
                neighbors = [neighbors c];
            end
            % Now we create recommendations and normalize them to be <= 1
            domRec(i, :) = sum(domSim{i}(seen, :), 1);
            domRec(i, ~ismember((1: size(domSim{i}, 1)), neighbors)) = 0;
            if max(domRec(i, :)) > 1
                domRec(i, :) = domRec(i, :)./max(domRec(i, :));
            end
        end
    end

    % Now we create the final recommendations, taking the mean value
    % between the ones in each domain
    recomList = mean(domRec, 1);

end