function [modelOUT] = createModel_copy (URM, param)
% simply COPY modelIN in modelOUT
%
% URM = matrix with user-ratings (NOT USED)
% param.modelIN = model

try
    modelOUT = param.modelIN;
catch
    error('createModel_copy: param.modelIN is required');
end