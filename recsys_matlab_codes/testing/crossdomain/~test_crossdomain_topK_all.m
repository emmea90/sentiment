function [] = test_crossdomain_topK_all  (  urmFull,...
                                            Ua,Ia,Ub,Ib,...
                                            percentageItemsInTest,...
                                            testPercentage,...
                                            mode )
% function [] = test_crossdomain_topK_all (urmFull,Ua,Ia,Ub,Ib,percentageItemsInTest,testPercentage,mode)
% 
% testPercentage = percentage of the test ratings that is to be tested
% mode = {'recall','fallout'}
%
% e.g.,
% test_crossdomain_topK_all(urm,1:floor(size(urm,1)/2),1:floor(size(urm,2)/2),floor(size(urm,1)/2)+1:size(urm,1),floor(size(urm,2)/2)+1:size(urm,2),0.5,1.0,'recall');

%% param check
if (nargin<8)
    help test_crossdomain_topK_all;
    return;
end

if ischar(mode)
    if ~(strcmpi(mode,'recall') || strcmpi(mode,'fallout'))
        error('mode can be either ''recall'' or ''fallout''');
    end
else
    error('mode can be either ''recall'' or ''fallout''');
end

%% test set extraction
addpath(genpath('algorithms'));
addpath(genpath('utilities'));
addpath(genpath('testing'));

numItemsInTestSet = floor(percentageItemsInTest*size(urmFull,2));
urmTraining=extractTrainingCrossDomain(urmFull,Ua,Ia,Ub,Ib);

testableItems = setdiff(1:size(urmFull,2),intersect(Ia,Ib));
if (numItemsInTestSet>length(testableItems))
    warning('number of items in test set is too large');
end

itemsToTest = randsample(testableItems,numItemsInTestSet);
tmpUrm=urmFull-urmTraining;
urmTest = sparse(size(urmFull,1),size(urmFull,2),0);
urmTest(:,itemsToTest) = tmpUrm(:,itemsToTest);
clear tmpUrm;

    display(['starting ', mode, ' tests']);
    if strcmpi(mode,'recall')
        [a,b]=find(urmTest==5); %recall
    else
        [a,b]=find(urmTest==1); %fallout
    end

    testSet=sparse(a,b,1);
    display(['size testSet=',num2str(nnz(testSet))]);

    [positiveTestsetReturn,negativeTestsetReturn]=extractTestSets (testSet,testPercentage,-1);

%% starting tests


%%
%%%%%% CROSS item-item transitiveClosure
disp(' CROSS item-item transitiveClosure');

    algoName = 'crossII_transClosure';
    fileNameRANK = ['rank',mode,algoName,'_','','.mat'];
    if fileExist(fileNameRANK) load(fileNameRANK); end 

    clear onlineparam modelparam;
    onlineparam.postProcessingFunction=@keep1000randomItems;
    onlineparam.filterViewedItems=true;
    modelparam.createAdditionalLinks=@transitiveClosure;
    tic
    [positiveTests,negativeTests]=leaveOneOut('algorithms/collaborative/neighborhoodBased/ItemItem_CrossDomain', @createModel_crossDomain_II, @onLineRecom_crossDomain_II,urmTraining,urmTraining,positiveTestsetReturn,negativeTestsetReturn,modelparam,onlineparam);
    eval([ '[rank',mode,algoName,'',']=computeRank(positiveTests);' ]); 
    save(fileNameRANK,['rank',mode,algoName,'*',' onlineparam modelparam']);
    toc    
    

%%    
%%%%%% MOVIE AVERAGE (non-personalized)
disp(' started MovieAvg');

    algoName = 'MovieAVG';
    fileNameRANK = ['rank',mode,algoName,'_','','.mat'];    
    if fileExist(fileNameRANK) load(fileNameRANK); end 
    
    clear onlineparam modelparam;
    onlineparam.postProcessingFunction=@keep1000randomItems;
    onlineparam.filterViewedItems=true;
    tic
    [positiveTests,negativeTests]=leaveOneOut('algorithms/non-personalized/movieAvg', @createModel_movieAvg, @onLineRecom_movieAvg,urmTraining,urmTraining,positiveTestsetReturn,negativeTestsetReturn,[],onlineparam);
    eval([ '[rank',mode,algoName,'',']=computeRank(positiveTests);' ]); 
    save(fileNameRANK,['rank',mode,algoName,'*',' onlineparam modelparam']);
    toc

%%
%%%%%% TOP RATED
disp(' started TopRated');

    algoName = 'TopRated';
    fileNameRANK = ['rank',mode,algoName,'_','','.mat'];
    if fileExist(fileNameRANK) load(fileNameRANK); end 

    clear onlineparam modelparam;
    onlineparam.postProcessingFunction=@keep1000randomItems;
    onlineparam.filterViewedItems=true;
    tic
    [positiveTests,negativeTests]=leaveOneOut('algorithms/non-personalized/topRated', @createModel_toprated, @onLineRecom_toprated,urmTraining,urmTraining,positiveTestsetReturn,negativeTestsetReturn,1,onlineparam);
    eval([ '[rank',mode,algoName,'',']=computeRank(positiveTests);' ]); 
    save(fileNameRANK,['rank',mode,algoName,'*',' onlineparam modelparam']);
    toc
     

%%    
%%%%%%% NNCosNgbr
disp(' started NNCOSNgbr knn');  

    knnn=[100];

    algoName = 'COS';
    fileNameRANK = ['rank',mode,algoName,'_','','.mat'];
    if fileExist(fileNameRANK) load(fileNameRANK); end 
    
    clear onlineparam modelparam;
    onlineparam.postProcessingFunction=@keep1000randomItems;
    onlineparam.filterViewedItems=true;
for i=1:length(knnn)
    tic
    modelparam.knn=knnn(i)
    [positiveTests,negativeTests]=leaveOneOut('algorithms/collaborative/neighborhoodBased/ItemItem_cosineKNN', @createModel_cosineIIknn, @onLineRecom_NNCosNgbr_II_knn,urmTraining,urmTraining,positiveTestsetReturn,negativeTestsetReturn,modelparam,onlineparam);
    eval([ '[rank',mode,algoName,'',']=computeRank(positiveTests);' ]); 
    save(fileNameRANK,['rank',mode,algoName,'*',' onlineparam modelparam']);    
    toc
end


%%    
%%%%%%% AsySVD
disp(' started AsySVD');  

    lss=[50];

    algoName = 'AsySVD';
    fileNameRANK = ['rank',mode,algoName,'_','','.mat'];
    if fileExist(fileNameRANK) load(fileNameRANK); end 
    
    clear onlineparam modelparam;
    onlineparam.postProcessingFunction=@keep1000randomItems;
    onlineparam.filterViewedItems=true;
for i=1:length(lss)
    tic
    modelparam.ls=lss(i)
    %modelAsySVD.iterations=1;
    [positiveTests,negativeTests]=leaveOneOut('algorithms/collaborative/latentFactors/AsySVD', @createModel_AsySVD, @onLineRecom_AsySVD,urmTraining,urmTraining,positiveTestsetReturn,negativeTestsetReturn,modelparam,onlineparam);
    eval([ '[rank',mode,algoName,'',']=computeRank(positiveTests);' ]); 
    save(fileNameRANK,['rank',mode,algoName,'*',' onlineparam modelparam']);     
    toc
end


% % %%
% % %%%%%%%% LOAD and PLOT
% % fdir=dir('rank*.mat');
% % for ff=1:length(fdir); load(fdir(ff).name); end
% % cdfplotRecallallRank;
% % ylabel(mode);
% % xlabel('N');

end