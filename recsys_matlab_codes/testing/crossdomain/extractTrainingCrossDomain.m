function [urmTraining] = extractTrainingCrossDomain(urm,Ua,Ia,Ub,Ib)
% urmTraining = extractTrainingCrossDomain(urm,Ua,Ia,Ub,Ib)

splitSize=1000;
cols=size(urm,2);
%completed = false;

urmTraining=sparse(size(urm,1),size(urm,2),0);
elSplits = ceil(cols/splitSize);
for i=1:elSplits
    maxNumOfEl=min([i*splitSize,cols]);
	indexes=splitSize*(i-1)+1:maxNumOfEl;   
    [r,c,v]=find(urm(:,indexes));
    cc=c+indexes(1) - 1;
    condition = (ismember(r,Ua) & ismember(cc,Ia)) | (ismember(r,Ub) & ismember(cc,Ib));
    urmTraining(:,indexes)= sparse([size(urm,1); r(condition)],[length(indexes); c(condition)] ,[0; v(condition)]);
end

%ind=sub2ind(size(urm),r(condition),c(condition));
% 
% while (~completed)
%     
%     try
%         elSplits = ceil(length(ind)/splitSize);
%         for i=1:elSplits
%             maxNumOfEl=min([i*splitSize,length(ind)]);
%             indexes=splitSize*(i-1)+1:maxNumOfEl;
%             urmTraining(ind(indexes)) = urm(ind(indexes));
%         end
%         completed=true;
% 
%     catch e
%         splitSize=0.5*splitSize;
%         display(['iteration=',num2str(i),' - splitSize=',num2str(splitSize)]);
%     end
end