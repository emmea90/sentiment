function [recomListFiltered] = keepXrandomItems (recomList,param)
% [recomListFiltered] = keepXrandomItems (recomList,param)
% - recomList=vector (length: number of items) with predicted ratings for
% each item.
% - param.itemToTest = item being tested. The function keeps X random
% items plus the itemToTest.
% - param.numberOfItems = the number X of items for creating the
% recommendation list
% - [param.filterViewedItems] = optional parameter which enables the use of
% the next following (param.viewedItems)
% - [param.viewedItems] = optional parameter which lists the items belonging 
% to the user profile. If the parameter is given 
% (and the optinal param.filterViewedItems is true) the X-random items 
% are selected from the set {allItems \ viewedItems}, instead of from the
% set of allItems.
%
% - [param.conditionalRecommendableItems] = optional parameters that lists the set of
% items that can be recommended. The X random items will be selected among
% this subset of recommendable items.
% This input is an array of struct('condition','itemSet'), where condition
% is the set of 'test items' the rule applies, while item set is the set of
% recommendable items. 
% E.g., param.conditionalRecommendableItems=[struct('condition',1:1000,'itemSet',1:100),struct('condition',1001:2000,'itemSet',101:200)];
% means that if the item to test is in the range [1 1000], the set of recommendable items is [1 100] 
%
    
if (~isstruct(param))
   error('param must be a struct!'); 
end

if (~isfield(param,'itemToTest'))
    error('param misses the required field: itemToTest');
end

if (~isfield(param,'numberOfItems'))
    error('param misses the required field: numberOfItems');
end

itemSet = [1:param.itemToTest-1, param.itemToTest+1:length(recomList)];

if (isfield(param,'filterViewedItems') && (param.filterViewedItems))
    if (isfield(param,'viewedItems'))
        itemSet=setdiff(itemSet,param.viewedItems);
    else
        warning('param misses the required field: filterViewedItems');
    end
end

if (isfield(param,'conditionalRecommendableItems'))
    if ~isempty(intersect( param.conditionalRecommendableItems.condition ))
        warning('keepXrandomItems:ovelappingRecommendableItemsConditions','param.conditionalRecommendableItems.condition are OVERLAPPED');
        warning('off','keepXrandomItems:ovelappingRecommendableItemsConditions');
    end
    for i=1:length(param.conditionalRecommendableItems)
        if ismember(param.itemToTest, param.conditionalRecommendableItems(i).condition)
            itemSet=intersect(itemSet,param.conditionalRecommendableItems(i).itemSet);
        end
    end
end
itemSet = intersect(itemSet,find(recomList~=-inf & ~isnan(recomList)));

recomListFiltered=ones(size(recomList))*(-inf);
try
    if length(itemSet)>=param.numberOfItems
        itemsToKeep=[param.itemToTest, randsample(itemSet, param.numberOfItems)];
        recomListFiltered(itemsToKeep)=recomList(itemsToKeep);
    else
        recomListFiltered=recomList;
    end
catch e
    display e;
end

end