function [ratings,users,items,density,userAvgLength,itemAvgLength] = analyseURMtimestamp(urmTimestamp,refdate,days)
% [ratings,users,items,density,userAvgLength,itemAvgLength] =
% analyseURMtimestamp(urmTimestamp,refdate,days)
%
% genera il grafico con l'evoluzione temporale dei rating della matrice urm
%
% urmTimestamp = matrice urm in formato csv: user,item, timestamp
% refdate = data di riferimento nel formato dd/mm/yyyy
% days = giorni di cui fare il plot (es: 1:1:105)
% 
% refdate e days sono opzionali, se assenti viene effettuato il plot di
% tutto l'asse temporale

if (nargin<2)
   refdate = datestr(min(urmTimestamp(:,3)),'dd/mm/yyyy');
   display(['Reference day: ',refdate]);
end
if (nargin<3)
   days = 1:1:(floor((max(urmTimestamp(:,3))-min(urmTimestamp(:,3)))));   
end

h= figure;

%for i=1:1:239
for i=days
    urm=generateURMfromTimestamp(urmTimestamp,datestr(addtodate(datenum(refdate,'dd/mm/yyyy'),i,'day'),'dd/mm/yyyy'));
    urm=compactURM(urm,3);
    
    if (nnz(urm)==0)
        break;
    end
    
    [users(i),items(i)]=size(urm);
    ratings(i)=nnz(urm); 
    urmSpones=spones(urm);
    usersLength=sum(urm,2);
    itemsLength=sum(urm,1);    
    userAvgLength(i)=mean(usersLength);    
    itemAvgLength(i)=mean(itemsLength);
    density=ratings./(users.*items);
    subplot(3,2,1); plot(ratings);
    subplot(3,2,2); plot(density);
    subplot(3,2,3); plot(userAvgLength);
    subplot(3,2,5); plot(itemAvgLength);
    subplot(3,2,4); plot(users);
    subplot(3,2,6); plot(items);
    if (mod(i,10)==0) drawnow; 
end
    subplot(3,2,1); title('# ratings'); grid on;
    subplot(3,2,2); title('density'); grid on;
    subplot(3,2,3); title('Avg user views'); grid on;
    subplot(3,2,5); title('Avg item views'); grid on;
    subplot(3,2,4); title('# users'); grid on;
    subplot(3,2,6); title('# items'); grid on;
end