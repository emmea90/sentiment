figure
hold on
plotRecallTSxaxis(density,recallTop,'k');
plotRecallTSxaxis(density,recallCos10,'r');
plotRecallTSxaxis(density,recallCos50,'r--');
plotRecallTSxaxis(density,recallCos100,'r-.');
plotRecallTSxaxis(density,recallSVD15,'b');
plotRecallTSxaxis(density,recallSVD25,'b--');
plotRecallTSxaxis(density,recallSVD50,'b-.');
plotRecallTSxaxis(density,recallSVD100,'b.-');
grid on
legend('toprated','cos knn 10','cos knn 50','cos knn 100','svd 15','svd 25','svd 50','svd 100');