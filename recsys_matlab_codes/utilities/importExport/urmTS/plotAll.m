function [] = plotAll(suffix,discardDays,considerDays,xAx)
%function [] = plotAll(suffix,discardDays,considerDays,xAx)

if nargin==0
    suffix='';
end

hFigure=figure( 'InvertHardcopy','off',...
                'Color',[1 1 1]);
axes1 = axes('Parent',hFigure,'FontSize',30);
hold on;

variablesVector = {'recallTop','recallCos10','recallCos100','recallSVD15','recallSVD100','recallSVD150'};
modeVector = {'k--','ok-','ok-.','sk-','sk-.','sk--'};
sizeLineVector = {2,3,3,3,3,3};
faceMarkerColVector = {'k','k','w','k','w','b'};
legendVector = {'toprated','cos knn 10','cos knn 100','svd 15','svd 100','svd 150'};

% variablesVector = {'recallTop','recallCos10','recallCos50','recallCos100','recallSVD15','recallSVD50','recallSVD100'};
% modeVector = {'k--','ok-','ok--','ok-.','sk-','sk--','sk-.'};
% sizeLineVector = {2,3,3,3,3,3,3};
% faceMarkerColVector = {'k','k','k','w','k','k','w'};
% legendVector = {'toprated','cos knn 10','cos knn 50','cos knn 100','svd 15','svd 50','svd 100'};

consideredIndexes=[];
for i=1:length(variablesVector)
    try
        yData = evalin('base',[variablesVector{i},suffix]);
        consideredIndexes=[consideredIndexes,i];
    catch
        continue;
    end
    if nargin>1 
        if length(discardDays)>0
            yData(discardDays)=0;
        end
    end
    if nargin>2 
        if length(considerDays)>0
            yData=yData(considerDays);
        end
    end    
    plottingFz = @plotRecallTS;
    twoInputs=false;
    if nargin>3 
        if length(xAx)>0
            xData = xAx;
            if length(considerDays)>0
                xData=xData(considerDays);
            end
            plottingFz = @plotRecallTSxaxis;
            twoInputs=true;
        end
    end   
    if (sum(yData)==0)
        consideredIndexes=consideredIndexes(1:end-1);
        continue;
    end
    if twoInputs
        plottingFz(xData,yData,modeVector{i},'LineWidth',sizeLineVector{i},'MarkerSize',15,'MarkerFaceColor',faceMarkerColVector{i});
    else
        plottingFz(yData,modeVector{i},'LineWidth',sizeLineVector{i},'MarkerSize',15,'MarkerFaceColor',faceMarkerColVector{i});  
    end
end
legend(legendVector(consideredIndexes));
box on;
if twoInputs
    xlabel(inputname(4),'FontSize',36);
else
    xlabel('Elapsed days','FontSize',36);
end
ylabel('Recall','FontSize',36);
% plotRecallTS(eval([,suffix]),'k');
% plotRecallTS(eval(['recallCos10',suffix]),'r');
% plotRecallTS(eval(['recallCos50',suffix]),'r--');
% plotRecallTS(eval(['recallCos100',suffix]),'r-.');
% plotRecallTS(eval(['recallSVD15',suffix]),'b');
% plotRecallTS(eval(['recallSVD25',suffix]),'b--');
% plotRecallTS(eval(['recallSVD50',suffix]),'b-.');
% plotRecallTS(eval(['recallSVD100',suffix]),'b.-');
%plotRecallTS(eval(['recallSVD150',suffix]),'b.-');
grid on
title(suffix);
hold off;
% legend('toprated','cos knn 10','cos knn 50','cos knn 100','svd 15','svd 25','svd 50','svd 100');

end