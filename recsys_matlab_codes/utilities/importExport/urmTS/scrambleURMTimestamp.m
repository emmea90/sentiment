function [urmScrambledTimestamp] = scrambleURMTimestamp(urmTimestamp)
% function [urm] = scrambleURMTimestamp(urmTimestamp)
%
% randomize the urm temporal collection of ratings by scrambling the
% timestamps
    
    indexes = randperm(size(urmTimestamp,1));
    
    urmScrambledTimestamp=[urmTimestamp(:,1),urmTimestamp(:,2),urmTimestamp(indexes,3)];
end