function [] = plotRecallTS(recall, varargin)
    if ~isempty(find(recall))
        plot(find(recall),recall(find(recall)),varargin{:});
    else
        plot(recall,varargin{:});
    end
end