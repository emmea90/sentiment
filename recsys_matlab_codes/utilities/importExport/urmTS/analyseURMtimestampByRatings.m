function [ratings,users,items,density,userAvgLength,itemAvgLength] = analyseURMtimestampByRatings(urmTimestamp,beginTS,endTS)
% [ratings,users,items,density,userAvgLength,itemAvgLength] =
% analyseURMtimestamp(urmTimestamp)

h=figure;

numUrmRatings=nnz(generateURMfromTimestamp(urmTimestamp,endTS,beginTS));
xx=500:500:numUrmRatings;
for i=1:length(xx)
    urm=generateURMfromTimestamp(urmTimestamp,endTS,beginTS,xx(i));
    urm=compactURM(urm,3);
    
    if (nnz(urm)==0)
        break;
    end
    
    [users(i),items(i)]=size(urm);
    ratings(i)=nnz(urm); 
    urmSpones=spones(urm);
    usersLength=sum(urm,2);
    itemsLength=sum(urm,1);    
    userAvgLength(i)=mean(usersLength);    
    itemAvgLength(i)=mean(itemsLength);
    density=ratings./(users.*items);
    subplot(3,2,1); plot(xx(1:i),ratings);
    subplot(3,2,2); plot(xx(1:i),density);
    subplot(3,2,3); plot(xx(1:i),userAvgLength);
    subplot(3,2,5); plot(xx(1:i),itemAvgLength);
    subplot(3,2,4); plot(xx(1:i),users);
    subplot(3,2,6); plot(xx(1:i),items);
    if (mod(i,10)==0) drawnow; 
end
    subplot(3,2,1); title('# ratings'); grid on;
    subplot(3,2,2); title('density'); grid on;
    subplot(3,2,3); title('Avg user views'); grid on;
    subplot(3,2,5); title('Avg item views'); grid on;
    subplot(3,2,4); title('# users'); grid on;
    subplot(3,2,6); title('# items'); grid on;
end