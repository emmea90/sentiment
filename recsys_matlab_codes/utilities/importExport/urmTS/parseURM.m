function [urmTS] = parseURM(filePath, userID_row, itemID_row, allocatedRows, dataParser)
% function [urmTS] = parseURM(filePath, userID_row, itemID_row, allocatedRows, dataParser)
%
% parse URM with timestamp (text file with 3 cols, userid|itemid|timestamp)
%
% PARAMETERS:
% filePath = path of urm text file
% allocatedRows = number of estimated rows (preallocated to speed up
% loading)
% dataParser = 'dd-mm-yyyy HH:MM:SS' oppure 'yyyy-nn-dd HH:MM:SS'

    fid = fopen(filePath);
    DELIMITER = '|';

    urmTS=zeros(allocatedRows,3);
    h = waitbar(0,'Please wait...');

    firstline=fgetl(fid);

    i=1;
    while 1
        fline = fgetl(fid);
        if ~ischar(fline),   break,   end
        splitted_indices = strfind(fline,DELIMITER);
        urmTS(i,1)= userID_row(find(userID_row(:,1)==str2num(fline(1:splitted_indices(1)-1))),2);
        urmTS(i,2)= itemID_row(find(itemID_row(:,1)==str2num(fline(splitted_indices(1)+1:splitted_indices(2)-1))),2);
        strdata = fline(splitted_indices(2)+1:end);
        if length(strdata)<12
            urmTS(i,3)= datenum(strdata,dataParser(1:10));
        else
            if length(strdata)>19 
                strdata=strdata(1:19);
            end
            urmTS(i,3)= datenum(strdata,dataParser);
        end
        i=i+1;
        if mod(i,5000)==0
            waitbar(i/allocatedRows,h,num2str(i))
        end
    end
    fclose(fid);

end