function [] = plotRecallTSxaxis(xaxes, recall, varargin)

    xax = xaxes(find(recall));
    yax = recall(find(recall));
    
    [values,indexes]=sort(xax);

    plot(xax(indexes),yax(indexes),varargin{:});
end