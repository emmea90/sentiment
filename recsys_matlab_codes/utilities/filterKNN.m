function [matrix] = filterKNN (matrix, knn, diagonal, wb)
% function [matrix] = filterKNN (matrix, knn, [diagonal], [wb])
%
% apply a knn filter to 'matrix', i.e., keep the largest 'knn' values 
% for each column. The output is a sparse matrix.
%
% the parameter 'diag' is optional. If 'diag' is true (1) all elements in 
% the main diagonal are set to zero. Default value is TRUE.
% 
% 'wb' specifies whether to show or not the waitbar

if (nargin<3)
    diagonal = true;
end

if (nargin<4)
    wb = false;
end

    if (diagonal)
        try
            matrix=matrix-diag(diag(matrix)); % annullo diagonale
            diagonal=false;
        end
    end
    %II = sparse(size(matrix,1),size(matrix,2)); % creo matrice vuota (sparsa)
    itemToKeep=zeros(1,knn*size(matrix,2));
    rows=zeros(1,knn*size(matrix,2));
    cols=zeros(1,knn*size(matrix,2));
    if (wb)
        h=waitbar(0,'computing knn'); cents=ceil(size(matrix,2)/100);
    end
    working=true;
    for i=1:size(matrix,2)
       colItem = matrix(:,i);
       if (working)
           try
                [r c] = sort(colItem,1,'descend');
           catch e
                display(['warning in filterKNN, column ',num2str(i)]);
                working=false;
                % display(e);
           end
       end
       if (~working)
           [r c] = sort(full(colItem),1,'descend');
       end
       itemToKeep(1+(i-1)*knn:i*knn) = r(1:knn);
       rows(1+(i-1)*knn:i*knn) = c(1:knn);
       cols(1+(i-1)*knn:i*knn) = ones(1,knn)*i;
       %II(itemToKeep,i) = matrix(itemToKeep,i);
       if (wb) && (mod(i,cents)==0)
            waitbar(i/size(matrix,2),h);
       end
    end
    matrix = sparse(rows,cols,itemToKeep,size(matrix,1),size(matrix,2));
    if (diagonal)
        try
            matrix=matrix-diag(diag(matrix)); % annullo diagonale se non annullata precedentemente
        catch
            diagmat=sparse(1:size(matrix,1),1:size(matrix,2),diag(matrix),size(matrix,1),size(matrix,2));
            matrix=matrix-diagmat;
        end
    end
    %matrix = II; % return
    if (wb)
        close(h);
    end

end