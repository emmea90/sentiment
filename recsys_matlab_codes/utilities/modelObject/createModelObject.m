function [modelObject] = createModelObject (titleFullFileName, icmFileName, modelFileName, dictionaryFileName, itemcacheFileName, modelStoreField)
% [modelObject] = createModelObject (titleFullFileName, icmFileName, modelFileName, dictionaryFileName, itemcacheFileName, [modelStoreField])
%
% e.g.,
% m_content=createModelObject('TitleFull_en.dat','bicm.mm','prjitem_lsa.mm','dictionary.txt','ITEMSCACHE.mm','dnorm');
% m_collaborative=createModelObject('TitleFull_en.dat','bicm.mm','vt.mm','dictionary.txt','ITEMSCACHE.mm','vt');
% m_collabKnn=createModelObject('TitleFull_en.dat','bicm.mm','itemitemk.mm','dictionary.txt','ITEMSCACHE.mm','II');
% 

if ~isempty(titleFullFileName)
    try 
        [a,b,modelObject.titles]=parseItemTitleDat(titleFullFileName);
        display('TitleFull correctly imported');
    catch
        display('Error while importing TitleFull');
    end
else
    display('TitleFull filename not specified');
end
    
if ~isempty(icmFileName)
    try
        modelObject.icm=mmread(icmFileName); 
        display('ICM correctly imported');
    catch
        display('Error while importing ICM');
    end
else
    display('ICM filename not specified');
end

if ~isempty(modelFileName)
    try
        if nargin>5
            modelObject.model = struct();
            modelObject.model = setfield (modelObject.model, modelStoreField, mmread(modelFileName));
            modelObject.modelStoreField = modelStoreField;
        else
            modelObject.model=mmread(modelFileName);
        end
        display('Model correctly imported');
    catch
        display('Error while importing Model');
    end
else
    display('Model filename not specified');
end

if ~isempty(itemcacheFileName)
    try
        modelObject.itemcache=parseItemCache(itemcacheFileName);
    catch
        display('Error while importing ITEMCACHE');
    end
else
    display('ITEMCACHE filename not specified');
end

if ~isempty(dictionaryFileName)
    try
    [stemRow,isSinonim,stem,isStem,lang,stemType,modelObject.dictionary]=parseDictionary2(dictionaryFileName);
        display('Dictionary correctly imported');
    catch
        try
            [stemRow,isSinonim,stem,isStem,lang,stemType,modelObject.dictionary]=parseDictionary(dictionaryFileName);
            display('Dictionary correctly imported');
        catch
            display('Error while importing Dictionary');
        end
    end
else
    display('Dictionary filename not specified');
end