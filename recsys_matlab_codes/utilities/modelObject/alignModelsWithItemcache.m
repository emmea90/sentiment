function [globalItemCache,globalTitles,globalICM,globalDictionary,varargout] = alignModelsWithItemcache (varargin)
% [globalItemCache,globalTitles,globalICM,globalDictionary,varargout] = alignModelsWithItemcache (varargin)
%
% [globalItemCache,globalTitles,globalICM,globalDictionary,model_collaborative,model_collabKnn,model_content,model_contentKnn]=alignModelsWithItemcache(m_collaborative,m_collabKnn,m_content,m_contentKnn);
%
distinctItemID = [];
distinctDictionaryStem = {};

for i=1:nargin
    if isfield(varargin{i},'dictionary')
        varargin{i}.dictionary = modifydictionary(varargin{i}.dictionary);
    end
end

for i=1:nargin
    itemcache = varargin{i}.itemcache;
    distinctItemID = union(distinctItemID, itemcache(:,1));
    
    if isfield(varargin{i},'dictionary')
        dictionary = varargin{i}.dictionary;
        originalStems = dictionary.stem;
        distinctDictionaryStem = union(distinctDictionaryStem, originalStems);
    end
end

distinctDictionaryStemTxt = char(distinctDictionaryStem);

globalItemCache = [distinctItemID, [1:length(distinctItemID)]'];

%itemID
for i=1:nargin
    outputModel = struct();
    
    itemcache = varargin{i}.itemcache;
    warning('off','GETINDEXFROMID:IDNOTFOUND');
    indexes = getIndexFromID(itemcache,distinctItemID);
    
    existingIDs = find(indexes~=-1);
    indexes=indexes(existingIDs);
    
    itemcache=[distinctItemID(existingIDs),existingIDs];
    outputModel.itemcache=itemcache;
    
    if isfield(varargin{i},'modelStoreField')
        itemModelOriginal = getfield(varargin{i}.model, varargin{i}.modelStoreField);
    else
        itemModelOriginal = getfield(varargin{i}.model);
    end
    if issparse(itemModelOriginal)
        itemModel = sparse(size(itemModelOriginal,1),length(distinctItemID));
    else
        itemModel = zeros(size(itemModelOriginal,1),length(distinctItemID));
    end
    itemModel(:,existingIDs) = itemModelOriginal(:,indexes);
    if (size(itemModelOriginal,1)==size(itemModelOriginal,2))
        if (numel(itemModel)<length(distinctItemID)^2)
            itemModel(length(distinctItemID),length(distinctItemID))=0;
        end
        itemModel(existingIDs,:) = itemModel(indexes,:);
    end

    if isfield(varargin{i},'modelStoreField')
        outputModel.model = struct();
        outputModel.model = setfield(outputModel.model, varargin{i}.modelStoreField, itemModel);
    else
        outputModel.model = itemModel;
    end    
    
    titlesOriginal=varargin{i}.titles;
    titles=char(zeros(length(distinctItemID),size(titlesOriginal,2)));
    titles(existingIDs,:)=titlesOriginal(indexes,:);
    outputModel.titles=titles;
    
    if isfield(varargin{i},'dictionary')
        dictionaryOriginal = varargin{i}.dictionary;
        icmOriginal = varargin{i}.icm;
        try
            icm=sparse(size(icmOriginal,1),length(distinctItemID));
            icm(:,existingIDs) = icmOriginal(:,indexes);
            if numel(icm)<(length(distinctDictionaryStem)*length(distinctItemID))
                icm(length(distinctDictionaryStem),length(distinctItemID))=0;
            end
        catch
            display('error in icm');
        end

        stemIndex=zeros(length(dictionaryOriginal.stem),1);
        h = waitbar(0,'Please wait...');
        dictionaryOriginalStemTxt=char(dictionaryOriginal.stem);
        stemIndexCur=1;
        for j=1:size(dictionaryOriginalStemTxt,1)
            stem = dictionaryOriginalStemTxt(j,:);
            startingIndex=stemIndexCur;
            stemIndexCur=-1;
            for q=[startingIndex+1:size(distinctDictionaryStemTxt,1) 1:startingIndex]
                if (strmatch(stem,distinctDictionaryStemTxt(q,:),'exact'))
                    stemIndexCur = q;
                    break;
                end
            end
            %stemIndexCur = strmatch(stem,distinctDictionaryStemTxt,'exact');
            if stemIndexCur>0
                stemIndex(j) = stemIndexCur;
            else
                warning([num2str(j),' - stem ', stem, ' not found']);
            end
            if mod(j,100)==0
                %waitbar(j/size(dictionaryOriginalStemTxt,1),h,num2str(j));
                waitbar(j/size(dictionaryOriginalStemTxt,1),h,'adapting dictionary');
            end
        end
        close(h);
        icm(stemIndex,:)=icm(1:size(icmOriginal,1),:);
        fields=fieldnames(dictionaryOriginal);
        for z=1:length(fields)
            tmpVar=getfield(dictionaryOriginal,fields{z});
            tmpVar(stemIndex,:)=tmpVar;
            dictionary=setfield(dictionary,fields{z},tmpVar);
        end
        outputModel.icm=icm;
        outputModel.dictionary=dictionary;
    end
    outputModel.modelStoreField=varargin{i}.modelStoreField;
    
    varargout{i} = outputModel;
end

h = waitbar(0,'Please wait...');
globalTitles = char(zeros(length(distinctItemID), size(titlesOriginal,2)));
globalICM = icm;
for j=1:length(distinctItemID)
    for i=1:nargin
        if isempty(strmatch('',varargout{i}.titles(j,:),'exact'))
            globalTitles(j,:)=varargout{i}.titles(j,:);
            break
        end
    end
    if isempty(globalICM(:,j))
        for i=1:nargin
            if ~isempty(varargout{i}.icm(:,j))
                globalICM(:,j) = varargout{i}.icm(:,j);
                break
            end
        end
    end    
    if mod(j,100)==0
        %waitbar(j/size(length(distinctItemID),1),h,num2str(j));
        waitbar(j/size(length(distinctItemID),1),h,'creating ICM');
    end        
end
close(h);

h = waitbar(0,'Please wait...');
globalDictionary = dictionary;
for i=1:length(distinctDictionaryStemTxt)
    strStem = distinctDictionaryStemTxt(i,:);
    splittedStr = strsplit('.',strStem);
    try
        if length(splittedStr)>4
            globalDictionary.stem(i)={[splittedStr{1:end-3}]};
        else
            globalDictionary.stem(i)=splittedStr(1);
        end
        globalDictionary.lang(i)=splittedStr(end-2);
        globalDictionary.stemType(i)=splittedStr(end-1);
        globalDictionary.isSinonim(i)=str2num(splittedStr{end});
    catch
        display(num2str(i));
    end
    if mod(i,100)==0
        %waitbar(i/size(length(distinctDictionaryStemTxt),1),h,num2str(i));
        waitbar(i/size(length(distinctDictionaryStemTxt),1),h,'creating dictionary');
    end    
end
close(h);


end


function [ dictionaryOut ] = modifydictionary (dictionaryIn)
    h = waitbar(0,'Please wait...');
    dictionaryOut = dictionaryIn;
    withisstem = isfield(dictionaryIn,'isStem');
    for c=1:length(dictionaryIn.stem)
        if withisstem
            dictionaryOut.stem(c) = {[dictionaryIn.stem{c},'.',dictionaryIn.lang{c},'.',dictionaryIn.stemType{c},'.',num2str(dictionaryIn.isSinonim(c)),'.',num2str(dictionaryIn.isStem(c))]};
        else
            dictionaryOut.stem(c) = {[dictionaryIn.stem{c},'.',dictionaryIn.lang{c},'.',dictionaryIn.stemType{c},'.',num2str(dictionaryIn.isSinonim(c))]};
        end
        if mod(c,100)==0
            %waitbar(c/length(dictionaryIn.stem),h,num2str(c));
            waitbar(c/length(dictionaryIn.stem),h,'converting dictionary');
        end        
    end
    close(h);
end