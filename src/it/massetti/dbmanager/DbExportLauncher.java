package it.massetti.dbmanager;

import it.massetti.filehandler.FileManager;
import it.massetti.movieelements.Film;
import it.massetti.movieelements.User;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class DbExportLauncher {

	public static void main (String[] args) throws SQLException, IOException {
		DbManager manager = new DbManager();
		manager.connectToDatabase("root", "");
		FileManager importer = new FileManager();
		List<Film> filmList = importer.obtainUpdatedList();
		List<User> userList = importer.obtainUserList();
		manager.exportData(filmList, userList);
	}
}
