package it.massetti.dbmanager;

import it.massetti.filehandler.MovieHandler;
import it.massetti.filehandler.UserNotFoundException;
import it.massetti.movieelements.AlternateIds;
import it.massetti.movieelements.Cast;
import it.massetti.movieelements.Film;
import it.massetti.movieelements.Links;
import it.massetti.movieelements.ListReviews;
import it.massetti.movieelements.Ratings;
import it.massetti.movieelements.ReleaseDates;
import it.massetti.movieelements.Review;
import it.massetti.movieelements.ReviewLinks;
import it.massetti.movieelements.User;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;



public class DbManager {
	
	private Connection con;
	private int releaseDateId=1;
	private int ratingId=1;
	private int castId=1;
	private int charachterId=1;
	private int alternateIdsId=1;
	private int linkId=1;
	private int reviewId=1;
	private int filmId=1;
	private MovieHandler handler;
	private String resetPath = "db/GenerationScript.sql";
	private PreparedStatement filmStatement;
	private PreparedStatement userStmt;
	private PreparedStatement reviewStatement;
	private PreparedStatement linkStatement;
	private PreparedStatement revLinkStatement;
	private PreparedStatement altIdStatement;
	private PreparedStatement castStatement;
	private PreparedStatement filmCharStatement;
	private PreparedStatement ratingStatements;
	private PreparedStatement releaseDateStatement;

	public void connectToDatabase(String username, String password) throws SQLException {
	    con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sentimentdb", username, password);
	    con.setAutoCommit(false);
	}

	public void exportData(List<Film> filmList, List<User> userList) throws SQLException, FileNotFoundException, IOException {
		try {
			handler = new MovieHandler(filmList, userList);
		} catch (IOException e) {
			System.out.println("Can't open file");
		}
		resetDatabase();
		initializeStatements();
		updateUserTable(userList);
		updateFilmTable(filmList);
		executeStatements();
		System.out.println("Export end");
		con.commit();
		con.close();
	}
	
	private void initializeStatements() throws SQLException {
		filmStatement = con.prepareStatement(
				   "INSERT INTO films (id, rotten_id, title, year, mpaa_rating, runtime, critics_consensus, synopsis, poster) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		userStmt = con.prepareStatement(
				   "INSERT INTO review_users (id, name) VALUES (?, ?)");
		reviewStatement = con.prepareStatement(
				   "INSERT INTO reviews (id, films_id, user_id, critic, date, freshness, publication, quote, original_score, converted_score, sentiment_rating) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");	
		linkStatement = con.prepareStatement(
				   "INSERT INTO film_links (id, films_id, self, alternate, cast, clips, reviews, similar) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
		revLinkStatement = con.prepareStatement(
				   "INSERT INTO review_links (reviews_id, reviewlink) VALUES (?, ?)");
		altIdStatement = con.prepareStatement(
				   "INSERT INTO film_alternate_ids (id, films_id, imdb) VALUES (?, ?, ?)");
		castStatement = con.prepareStatement(
				   "INSERT INTO film_abridged_casts (id, films_id, name) VALUES (?, ?, ?)");
		filmCharStatement = con.prepareStatement(
				   "INSERT INTO film_characters (id, abriged_cast_id, charachter) VALUES (?, ?, ?)");
		ratingStatements = con.prepareStatement(
				   "INSERT INTO film_ratings (id, films_id, critics_rating, critics_score, audience_rating, audience_score) VALUES (?, ?, ?, ?, ?, ?)");
		releaseDateStatement = con.prepareStatement(
				   "INSERT INTO film_release_dates (id, films_id, theater, dvd)  VALUES (?, ?, ?, ?)");
	}
	
	private void executeStatements() throws SQLException {
		filmStatement.executeBatch();
		userStmt.executeBatch();
		reviewStatement.executeBatch();
		linkStatement.executeBatch();
		revLinkStatement.executeBatch();
		altIdStatement.executeBatch();
		castStatement.executeBatch();
		filmCharStatement.executeBatch();
		ratingStatements.executeBatch();
		releaseDateStatement.executeBatch();
	}

	private void updateFilmTable(List<Film> filmList) throws SQLException {
		for(Film current : filmList) {
			int id = current.getId();
			String title = current.getTitle();
			int year = current.getYear();
			String mpaa_rating = current.getMpaa_rating();
			int runtime = current.getRuntime();
			String critics_consensus = current.getCritics_consensus();
			String synopsis = current.getSynopsis();
			String poster = current.getPosters().getOriginal();
			filmStatement.setInt(1, filmId);
			filmStatement.setInt(2, id);
			filmStatement.setString(3, title);
			filmStatement.setInt(4, year);
			filmStatement.setString(5, mpaa_rating);
			filmStatement.setInt(6, runtime);
			filmStatement.setString(7, critics_consensus);
			filmStatement.setString(8, synopsis);
			filmStatement.setString(9, poster);
			displayStatement(filmStatement);
			filmStatement.addBatch();
			insertReleaseDates (current.getRelease_dates(), filmId);
			insertRatings (current.getRatings(), filmId);
			insertCast (current.getAbridged_cast(), filmId);
			insertAlternateIds (current.getAlternate_ids(), filmId);
			insertLinks (current.getLink(), filmId);
			insertReviews (current.getListreviews(), filmId);
			filmId++;
		}
	}

	private void updateUserTable(List<User> userList) throws SQLException {
		for(User current : userList) {
			int id = current.getId();
			String name = current.getName();
			userStmt.setInt(1, id);
			userStmt.setString(2, name);
			displayStatement(userStmt);
			userStmt.addBatch();
		}
	}

	private void displayStatement (PreparedStatement pstmt) {
		System.out.println(pstmt);

	}
	private void resetDatabase () throws FileNotFoundException, IOException, SQLException {
		ScriptRunner runner = new ScriptRunner(con, false, true);
		runner.runScript(new BufferedReader(new FileReader(resetPath)));
	}

	private void insertReviews(ListReviews listreviews, int id) throws SQLException {
		for (Review current : listreviews.getReviews()) {
			String critic = current.getCritic();
			Integer criticId;
			try {
				criticId = handler.findId(critic);
			} catch (UserNotFoundException e) {
				criticId = null;
			}
			String date = current.getDate();
			String freshness = current.getFreshness();
			String publication = current.getPublication();
			String quote = current.getQuote();
			String original_score = current.getOriginal_score();
			Double converted_score = current.getConverted_score();
			Integer sentiment_rating = current.getSentimentRating();
			reviewStatement.setInt(1, reviewId);
			reviewStatement.setInt(2, id);
			try {
				reviewStatement.setInt(3, criticId);
			} catch (NullPointerException e) {
				reviewStatement.setInt(3, 0);
			}
			reviewStatement.setString(4, critic);
			reviewStatement.setString(5, date);
			reviewStatement.setString(6, freshness);
			reviewStatement.setString(7, publication);
			reviewStatement.setString(8, quote);
			reviewStatement.setString(9, original_score);
			reviewStatement.setDouble(10, converted_score);
			reviewStatement.setInt(11, sentiment_rating);
			displayStatement(reviewStatement);
			reviewStatement.addBatch();
			insertReviewLinks(current.getLinks(), reviewId);
			reviewId++;
		}
	}

	private void insertReviewLinks(ReviewLinks links, int currentReviewId) throws SQLException {
		String review = links.getReview();
		revLinkStatement.setInt(1, currentReviewId);
		revLinkStatement.setString(2, review);
		revLinkStatement.addBatch();
	}

	private void insertLinks(Links link, int id) throws SQLException {
		String self = link.getSelf();
		String alternate = link.getAlternate();
		String cast = link.getCast();
		String clips = link.getClips();
		String reviews = link.getReviews();
		String similar = link.getSimilar();
		linkStatement.setInt(1, linkId);
		linkStatement.setInt(2, id);
		linkStatement.setString(3, self);
		linkStatement.setString(4, alternate);
		linkStatement.setString(5, cast);
		linkStatement.setString(6, clips);
		linkStatement.setString(7, reviews);
		linkStatement.setString(8, similar);
		displayStatement(linkStatement);
		linkStatement.addBatch();
		linkId++;
	}

	private void insertAlternateIds(AlternateIds alternate_ids, int id) throws SQLException {
		int imdb_id;
		try {
			imdb_id = alternate_ids.getImdb();
		} catch (NullPointerException e) {
			imdb_id = 0;
		}
		altIdStatement.setInt(1, alternateIdsId);
		altIdStatement.setInt(2, id);
		altIdStatement.setInt(3, imdb_id);
		displayStatement(altIdStatement);
		altIdStatement.addBatch();
		alternateIdsId++;
	}

	private void insertCast(List<Cast> abridged_cast, int id) throws SQLException {
		for (Cast actor : abridged_cast) {
			String name = actor.getName();
			castStatement.setInt(1, castId);
			castStatement.setInt(2, id);
			castStatement.setString(3, name);
			displayStatement(castStatement);
			castStatement.addBatch();
			try {
				insertCharacters(actor.getCharacters(), castId);
			} catch (NullPointerException e) {
				
			}
			castId++;
		}
	}

	private void insertCharacters(List<String> list, int id) throws SQLException {
		for (String charachter : list) {
			filmCharStatement.setInt(1, charachterId);
			filmCharStatement.setInt(2, id);
			filmCharStatement.setString(3, charachter);
			displayStatement(filmCharStatement);
			filmCharStatement.addBatch();
			charachterId++;
		}
	}

	private void insertRatings(Ratings ratings, int id) throws SQLException {
		String critics_rating = ratings.getCritics_ratings();
		int critics_score = ratings.getCritics_score();
		String audience_rating = ratings.getAudience_ratings();
		int audience_score = ratings.getAudience_score();
		ratingStatements.setInt(1, ratingId);
		ratingStatements.setInt(2, id);
		ratingStatements.setString(3, critics_rating);
		ratingStatements.setInt(4, critics_score);
		ratingStatements.setString(5, audience_rating);
		ratingStatements.setInt(6, audience_score);
		displayStatement(ratingStatements);
		ratingStatements.addBatch();
		ratingId++;
	}

	private void insertReleaseDates(ReleaseDates releaseDates, int id) throws SQLException {
		String theater = releaseDates.getTheater_date();
		String dvd = releaseDates.getDvd_date();
		releaseDateStatement.setInt(1, releaseDateId);
		releaseDateStatement.setInt(2, id);
		releaseDateStatement.setString(3, theater);
		releaseDateStatement.setString(4, dvd);
		displayStatement(releaseDateStatement);
		releaseDateStatement.addBatch();
		releaseDateId++;
	}

}
