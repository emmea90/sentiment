package it.massetti.movieelements;

public class ReviewLinks {
	private String review;

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}
}
