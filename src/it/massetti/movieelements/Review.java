package it.massetti.movieelements;


public class Review {
	private String critic;
	private String date;
	private String freshness;
	private String publication;
	private String quote;
	private String original_score;
	private double converted_score;
	private ReviewLinks links;
	private int sentimentRating;
	
	public String getCritic() {
		return critic;
	}
	public void setCritic(String critic) {
		this.critic = critic;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getFreshness() {
		return freshness;
	}
	public void setFreshness(String freshness) {
		this.freshness = freshness;
	}
	public String getPublication() {
		return publication;
	}
	public void setPublication(String publication) {
		this.publication = publication;
	}
	public String getQuote() {
		return quote;
	}
	public void setQuote(String quote) {
		this.quote = quote;
	}
	public ReviewLinks getLinks() {
		return links;
	}
	public void setLinks(ReviewLinks links) {
		this.links = links;
	}
	public int getSentimentRating() {
		return sentimentRating;
	}
	public void setSentimentRating(int rating) {
		this.sentimentRating = rating;
	}
	public String getOriginal_score() {
		return original_score;
	}
	public void setOriginal_score(String original_score) {
		this.original_score = original_score;
	}
	public double getConverted_score() {
		return converted_score;
	}
	public void setConverted_score(double converted_score) {
		this.converted_score = converted_score;
	}
}
