package it.massetti.movieelements;

import java.util.List;

public class Cast {
	private String name;
	private List<String> characters;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getCharacters() {
		return characters;
	}
	public void setCharachters(List<String> characters) {
		this.characters = characters;
	}
}
