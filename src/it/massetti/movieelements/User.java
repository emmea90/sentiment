package it.massetti.movieelements;

import java.util.ArrayList;
import java.util.List;

public class User {
	private String name;
	private int id;
	private List<Review> reviews;
	
	public User(String name, int id) {
		this.name = name;
		this.id = id;
		reviews = new ArrayList<Review>();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public List<Review> getReviews() {
		return reviews;
	}
	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}

	public void addReview(Review currentReview) {
		reviews.add(currentReview);	
	}
	
}
