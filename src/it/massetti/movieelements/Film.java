package it.massetti.movieelements;

import java.util.List;

public class Film {
	
	private int id;
	private String Title;
	private int Year;
	private String mpaa_rating;
	private int runtime;
	private String critics_consensus;
	private ReleaseDates release_dates;
	private Ratings ratings;
	private String synopsis;
	private Posters posters;
	private List<Cast> abridged_cast;
	private AlternateIds alternate_ids;
	private Links links;
	private ListReviews listReviews;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		this.Title = title;
	}
	public int getYear() {
		return Year;
	}
	public void setYear(int year) {
		this.Year = year;
	}
	public String getMpaa_rating() {
		return mpaa_rating;
	}
	public void setMpaa_rating(String mpaa_rating) {
		this.mpaa_rating = mpaa_rating;
	}
	public int getRuntime() {
		return runtime;
	}
	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}
	public String getCritics_consensus() {
		return critics_consensus;
	}
	public void setCritics_consensus(String critics_consensus) {
		this.critics_consensus = critics_consensus;
	}
	public ReleaseDates getRelease_dates() {
		return release_dates;
	}
	public void setRelease_dates(ReleaseDates release_dates) {
		this.release_dates = release_dates;
	}
	public Ratings getRatings() {
		return ratings;
	}
	public void setRatings(Ratings ratings) {
		this.ratings = ratings;
	}
	public String getSynopsis() {
		return synopsis;
	}
	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}
	public Posters getPosters() {
		return posters;
	}
	public void setPosters(Posters posters) {
		this.posters = posters;
	}
	public List<Cast> getAbridged_cast() {
		return abridged_cast;
	}
	public void setAbridged_cast(List<Cast> abridged_cast) {
		this.abridged_cast = abridged_cast;
	}
	public AlternateIds getAlternate_ids() {
		return alternate_ids;
	}
	public void setAlternate_ids(AlternateIds alternate_ids) {
		this.alternate_ids = alternate_ids;
	}
	public Links getLink() {
		return links;
	}
	public void setLink(Links link) {
		this.links = link;
	}
	public ListReviews getListreviews() {
		return listReviews;
	}
	public void setListreviews(ListReviews listreviews) {
		this.listReviews = listreviews;
	}
}
