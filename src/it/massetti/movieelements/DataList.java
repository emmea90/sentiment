package it.massetti.movieelements;

import java.util.ArrayList;
import java.util.List;


public class DataList {
	
	private List<Film> movies;
	private List<User> users;
	
	public DataList()
    {
        movies = new ArrayList<Film>();
    }

	public List<Film> getMovies() {
		return movies;
	}

	public void setMovies(List<Film> movies) {
		this.movies = movies;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
    

}
