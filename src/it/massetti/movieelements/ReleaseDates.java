package it.massetti.movieelements;

public class ReleaseDates {
	private String theater;
	private String DVD;
	
	public String getDvd_date() {
		return DVD;
	}
	public void setDvd_date(String dvd_date) {
		this.DVD = dvd_date;
	}
	public String getTheater_date() {
		return theater;
	}
	public void setTheater_date(String theater_date) {
		this.theater = theater_date;
	}
}
