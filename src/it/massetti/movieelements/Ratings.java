package it.massetti.movieelements;

public class Ratings {
	
	private String critics_rating;
	private int critics_score;
	private String audience_rating;
	private int audience_score;
	
	public String getCritics_ratings() {
		return critics_rating;
	}
	public void setCritics_ratings(String critics_rating) {
		this.critics_rating = critics_rating;
	}
	public int getCritics_score() {
		return critics_score;
	}
	public void setCritics_score(int critics_score) {
		this.critics_score = critics_score;
	}
	public String getAudience_ratings() {
		return audience_rating;
	}
	public void setAudience_ratings(String audience_rating) {
		this.audience_rating = audience_rating;
	}
	public int getAudience_score() {
		return audience_score;
	}
	public void setAudience_score(int audience_score) {
		this.audience_score = audience_score;
	}
}
