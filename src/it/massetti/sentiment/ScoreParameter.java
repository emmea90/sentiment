package it.massetti.sentiment;

public class ScoreParameter {
	private double neutralNegative;
	private double neutralPositive;
	private double quotient;
	private int accuracy;
	private int type;
	
	public ScoreParameter (double neutralNegative, double neutralPositive, double quotient, int type) {
		this.neutralNegative = neutralNegative;
		this.neutralPositive = neutralPositive;
		this.quotient = quotient;
		this.accuracy = 0;
		this.setType(type);
	}
	
	public double getNeutralNegative() {
		return neutralNegative;
	}
	public void setNeutralNegative(double neutralNegative) {
		this.neutralNegative = neutralNegative;
	}
	public double getNeutralPositive() {
		return neutralPositive;
	}
	public void setNeutralPositive(double neutralPositive) {
		this.neutralPositive = neutralPositive;
	}
	public double getQuotient() {
		return quotient;
	}
	public void setQuotient(double quotient) {
		this.quotient = quotient;
	}
	public int getAccuracy() {
		return accuracy;
	}
	public void setAccuracy(int accuracy) {
		this.accuracy = accuracy;
	}

	public void updateAccuracy(Integer sentimentScore, Integer realScore) {
		if(Math.abs(sentimentScore + realScore) == 0)		 {
			accuracy = accuracy +5;
		} else if(Math.abs(sentimentScore + realScore) == 1) {
			accuracy = accuracy -1;
		} else if (Math.abs(sentimentScore + realScore) == 2) {
			accuracy = accuracy -5;
		}
	}
	
	public double getScore () {
		if(neutralNegative!=0) {
			return neutralNegative;
		}
		if(neutralPositive!=0) {
			return neutralPositive;
		}
		return quotient;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
}
