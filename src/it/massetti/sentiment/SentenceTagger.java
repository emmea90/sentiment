package it.massetti.sentiment;

import java.io.File;
import java.io.IOException;

import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSSample;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.WhitespaceTokenizer;

public class SentenceTagger {
	POSModel model;
	POSTaggerME tagger;
	
	public SentenceTagger() {
		model = new POSModelLoader().load(new File("res/en-pos-maxent.bin"));
		tagger = new POSTaggerME(model);
	}

	public String process(String period) throws IOException {
		String whitespaceTokenizerLine[] = WhitespaceTokenizer.INSTANCE.tokenize(period);
		String[] tags = tagger.tag(whitespaceTokenizerLine);
		POSSample taggedPeriod = new POSSample(whitespaceTokenizerLine, tags);		
		return taggedPeriod.toString();
	}
}

/*
CC - Coordinating conjunction
CD - Cardinal number
DT - Determiner
EX - Existential there
FW - Foreign word
IN - Preposition or subordinating conjunction
JJ - Adjective
JJR - Adjective, comparative
JJS - Adjective, superlative
NN - Noun, singular or mass
NNS - Noun, plural
NNP - Proper noun, singular
NNPS - Proper noun, plural
PDT � Predeterminer
NP - Noun Phrase.
PP - Prepositional Phrase
VP - Verb Phrase.
PRP - Personal pronoun
RB - Adverb
RBR - Adverb, comparative
RBS - Adverb, superlative
RP - Particle
SYM - Symbol
TO - to
UH - Interjection
VB - Verb, base form
VBD - Verb, past tense
VBG - Verb, gerund or present participle
VBN - Verb, past participle
VBP - Verb, non-3rd person singular present
VBZ - Verb, 3rd person singular present
WDT - Wh-determiner
WP - Wh-pronoun
WRB - Wh-adverb 1 
*/