package it.massetti.sentiment;

import java.util.HashMap;
import java.util.Map;

public class ScoreManager {

	private Map<Integer, Integer> givenPoints;
	private Map<Integer, Integer> retrievedPoints;
	
	public ScoreManager () {
		givenPoints = new HashMap<Integer, Integer>();
		retrievedPoints = new HashMap<Integer, Integer>();
		for(int i=-1; i<=1; i++) {
			givenPoints.put(i, 0);
			retrievedPoints.put(i, 0);
		}
	}
	
	public void UpdateMaps (int rating, String type) {
		if (type=="Given") {
			updateValue(rating, givenPoints);
		} else {
			updateValue(rating, retrievedPoints);			
		}
	}
	
	private void updateValue(int rating, Map<Integer, Integer> mapToUpdate) {
		int numberOfRatings = mapToUpdate.get(rating);
		numberOfRatings++;
		mapToUpdate.put(rating, numberOfRatings++);
	}
	
	public void displayScores () {
		for(int i=-1; i<=1; i++) {
			System.out.println("Retrieved " + i + " = " + retrievedPoints.get(i));
		}	
		for(int i=-1; i<=1; i++) {
			System.out.println("Given " + i + " = " + givenPoints.get(i));
		}	
	}

	public Integer convertScore(String original_score) {
		Integer score = null;
		if(original_score.contains("[")) {
			return null;
		}
		if(original_score.contains("/")) {
			String[] fraction = original_score.split("/");
			Double partialScore = Double.parseDouble(fraction[0].replaceAll("[^0-9.]", ""))/Double.parseDouble(fraction[1].replaceAll("[^0-9.]", ""));
			if (partialScore > 0.7) {
				return 1;
			} else if (partialScore <= 0.25) {
				return -1;
			} else {
				return 0;
			}
		}
		if(original_score.contains("A")) {
			return 1;
		}
		if(original_score.contains("B")) {
			return 1;
		}
		if(original_score.contains("C")) {
			return 0;
		}
		if(original_score.contains("D")) {
			return -1;
		}
		if(original_score.contains("E")) {
			return -1;
		}
		return score;
	}
	
	
}
