package it.massetti.sentiment;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import opennlp.tools.cmdline.parser.ParserTool;
import opennlp.tools.parser.Parser;
import opennlp.tools.parser.Parse;
import opennlp.tools.parser.ParserFactory;
import opennlp.tools.parser.ParserModel;
import opennlp.tools.util.InvalidFormatException;

public class SentenceParser {
	InputStream is;
	ParserModel model;
	Parser parser;
	
	public SentenceParser() throws InvalidFormatException, IOException {
		is = new FileInputStream("res/en-parser-chunking.bin");
		model = new ParserModel(is);
		parser = ParserFactory.create(model);
	}

	public Parse[] process (String period) throws IOException {
		// http://sourceforge.net/apps/mediawiki/opennlp/index.php?title=Parser#Training_Tool		
		Parse topParses[] = ParserTool.parseLine(period, parser, 1);
		
		for (Parse current : topParses)
		{
			current.show();
		}
		is.close();
		
		return topParses;
	}
}
