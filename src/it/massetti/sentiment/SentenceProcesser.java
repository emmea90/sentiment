package it.massetti.sentiment;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;

public class SentenceProcesser {
	
	SentenceModel model;
	InputStream modelIn;
	
	public SentenceProcesser() throws FileNotFoundException {
		InputStream modelIn = new FileInputStream("res/en-sent.bin");
		try {
			  model = new SentenceModel(modelIn);
			}
			catch (IOException e) {
			  e.printStackTrace();
			}
			finally {
			  if (modelIn != null) {
			    try {
			      modelIn.close();
			    }
			    catch (IOException e) {
			    }
			  }
			}
	}
	
	public String[] process (String period) {
		SentenceDetectorME sentenceDetector = new SentenceDetectorME(model);
		String sentences[] = sentenceDetector.sentDetect(period);
		
		// Debug stuff
		for(String current : sentences) {
			System.out.println(current);
		}
		return sentences;
		
	}
}
