package it.massetti.sentiment;

import it.massetti.filehandler.Score;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class SentenceAnalyzer {
	
	final int MAX_SCALE = 5;
	final int MIN_SCALE = 0;
	
    String pathToSWN;
    double points;
    SentimentAnalyzer sentiwordnet;
    SentenceTagger tagger;
    
	public SentenceAnalyzer () throws IOException {
	    pathToSWN = "res/SentiWordnet.txt";
	    sentiwordnet = new SentimentAnalyzer(pathToSWN);
		tagger = new SentenceTagger();
	}

	public Integer analyze (String period) throws IOException {
		String[] splitted = prepareString(period);
		List<String[]> speeched = identifySpeeches(splitted);
		Integer rating = analyzeSpeech(speeched);
	    return rating;
	}
	
	/**
	 * Analyze the speech
	 * @param speeched
	 * @return
	 */
	public Integer analyzeSpeech (List<String[]> speeched) {
		Double senti_score = null;
		Double pos_score = null;
		Double neg_score = null;
		boolean previousnot = false;
	    for (String[] phrase : speeched) {
			try {
		    	if(phrase[0].toLowerCase().equals("not")) {
		    		previousnot = true;
		    		continue;
		    	} else {
		    		previousnot = false;		
		    	}
				senti_score = sentiwordnet.extract(phrase[0].toLowerCase(), phrase[1]);				
				if (previousnot) {
					senti_score = -senti_score;
				}
			
				if(senti_score > 0) {
					if (pos_score == null) {
						pos_score = 0.0;
					}
					pos_score = pos_score + senti_score;
				} else {
					if (neg_score == null) {
						neg_score = 0.0;
					}
					neg_score = neg_score + senti_score;
				}
			} catch (NullPointerException e) {
			}
	    }
	    System.out.println("Positive: " + pos_score);
	    System.out.println("Negative: " + neg_score);
	    Integer rating = normalize(pos_score, neg_score);
		return rating;
	}
	
	/**
	 * It normalize the score on a fixed rank
	 * @param pos_score
	 * @param neg_score
	 * @param objectivity
	 * @return
	 */
	private Integer normalize(Double pos_score, Double neg_score) {
		if(pos_score == null && neg_score == null) {
			return null;
		}
		if(pos_score == null || pos_score == 0) {
			if (-neg_score < 0.16) {
				return 0;
			} else {
				return -1;
			}
		}
		if(neg_score == null || neg_score == 0) {
			if (pos_score < 0.14) {
				return 0;
			} else {
				return 1;
			}
		}
		double total = 0;
		total = -neg_score + pos_score;
		if (total == 0) {
			return 0;
		}
		double quotient;
		if (pos_score > -neg_score) {
			quotient = pos_score / total;
		} else {
			quotient = -neg_score / total;
		}
		System.out.println("Quotient: " + quotient);
		System.out.println("Total: " + total);
	    if(quotient < 0.74) {
	    	return 0;
	    } else if(pos_score > -neg_score) {
    			return 1;    		
	    	} else {
	    		return -1;
	    }
	}

	/**
	 * Extracts all the adjectives
	 * @param splitted
	 * @return
	 */
	@SuppressWarnings("unused")
	private List<String[]> extractAdjective (List<String[]> splitted) {
		List<String[]> adjectives = new ArrayList<String[]>();
	    for (String[] phrase : splitted) {
	    	if(phrase[1].equals("a")) {
	    		adjectives.add(phrase);
	    	}
	    }
		return adjectives;
	}
	
	private List<String[]> identifySpeeches (String[] splitted) {
		List<String[]> speeched = new ArrayList<String[]>();
	    try {
			for (String lemma : splitted) {
				String[] wordBinomius = lemma.split("_");
				wordBinomius[1] = calculatePart(wordBinomius[1]);
				speeched.add(wordBinomius);
		    }
	    } catch (ArrayIndexOutOfBoundsException e) {
	    	
	    }
		return speeched;
	}
	
	private String[] prepareString (String period) throws IOException {
		// Extend the negations
		String negationExtendedPeriod = period.replaceAll("won't", "will not");
		negationExtendedPeriod = negationExtendedPeriod.replaceAll("n't", " not");
		// Create tags
		String taggedPeriod = tagger.process(negationExtendedPeriod);
		// Remove punctuations
		String linearPeriod = taggedPeriod.replaceAll("[^A-Za-z0-9 _']", "");
		// Split words
		String[] splited = linearPeriod.split("\\s+");
		return splited;
	}
	
	private String calculatePart(String type) {
		try {
			switch (type.substring(0, 2)) {
			case "JJ":
				return "a";
			case "NN":
				return "n";
			case "VB":
				return "v";
			case "RB":
				return "r";
			default:
				return null;
			}			
		} catch (StringIndexOutOfBoundsException e) {
			
		}
		return null;
	}

	public Score obtainDetailedScores(List<String[]> speeched) {
		Double senti_score = null;
		Double pos_score = null;
		Double neg_score = null;
		Integer pos_words = 0;
		Integer neg_words = 0;
		boolean previousnot = false;
	    for (String[] phrase : speeched) {
			try {
		    	if(phrase[0].toLowerCase().equals("not")) {
		    		previousnot = true;
		    		continue;
		    	} else {
		    		previousnot = false;		
		    	}
				senti_score = sentiwordnet.extract(phrase[0].toLowerCase(), phrase[1]);				
				if (previousnot) {
					senti_score = -senti_score;
				}
			
				if(senti_score > 0) {
					if (pos_score == null) {
						pos_score = 0.0;
					}
					pos_score = pos_score + senti_score;
					pos_words++;
				} else {
					if (neg_score == null) {
						neg_score = 0.0;
					}
					neg_score = neg_score + senti_score;
					neg_words++;
				}
			} catch (NullPointerException e) {
			}
	    }
	    if(pos_words > 0) {
	    	pos_score = pos_score/pos_words;
	    }
	    if(neg_words > 0) {
		    neg_score = neg_score/neg_words; 	
	    }
	    System.out.println("Positive: " + pos_score);
	    System.out.println("Negative: " + neg_score);
	    Double total = calculateTotal(pos_score, neg_score);
	    Double quotient = calculateQuotient(pos_score, neg_score, total);
	    Score ratingObtained = new Score(pos_score, neg_score, total, quotient);
		return ratingObtained;
	}
	
	private Double calculateTotal(Double pos_score, Double neg_score) {
		try {
			Double total = -neg_score + pos_score;
			return total;
		} catch (NullPointerException e) {
			return null;
		}
	}
	
	private Double calculateQuotient(Double pos_score, Double neg_score, Double total) {
		try {
			Double quotient;
			if (pos_score > -neg_score) {
				quotient = pos_score / total;
			} else {
				quotient = -neg_score / total;
			}
			return quotient;
		} catch (NullPointerException e) {
			return null;
		}
	}

	public Score analyzeToObtainDetailedScores(String quote) throws IOException {
		String[] splitted = prepareString(quote);
		List<String[]> speeched = identifySpeeches(splitted);
		Score rating = obtainDetailedScores(speeched);
	    return rating;
	}
	
	/**
	 * It normalize the score on a fixed rank and returns the score
	 * @param pos_score
	 * @param neg_score
	 * @param objectivity
	 * @return
	 */
	public Integer normalize(Score rating, Double i, Double j, Double k) {
		Double pos_score = rating.getPos_score();
		Double neg_score = rating.getNeg_score(); 
		switch (rating.getScoreType()) {
			case 0: 
			{
				if (-neg_score < i) {
					return 0;
				} else {
					return -1;
				}
			}
			case 1:
			{
				if(neg_score == null || neg_score == 0) {
					if (pos_score < j) {
						return 0;
					} else {
						return 1;
					}
				}
			}
			case 2:
			{
				double total = 0;
				total = -neg_score + pos_score;
				if (total == 0) {
					return 0;
				}
				double quotient;
				if (pos_score > -neg_score) {
					quotient = pos_score / total;
				} else {
					quotient = -neg_score / total;
				}
			    if(quotient < k) {
			    	return 0;
			    } else if(pos_score > -neg_score) {
		    			return 1;    		
			    	} else {
			    		return -1;
			    }
			}
			default:
			{
				return null;
			}

		}
	}

}
