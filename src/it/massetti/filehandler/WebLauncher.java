package it.massetti.filehandler;

import java.io.IOException;
import it.massetti.sentiment.SentenceAnalyzer;

public class WebLauncher {

	public static void main (String args[]) throws IOException {
		String period = "I loved that film. I will watch it again for sure! It's simply amazing!";
		SentenceAnalyzer analyzer = new SentenceAnalyzer();
		int rating = analyzer.analyze(period);
		System.out.println("Rating: " + rating);
	}
}
