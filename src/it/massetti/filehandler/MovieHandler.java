package it.massetti.filehandler;

import it.massetti.movieelements.Film;
import it.massetti.movieelements.Review;
import it.massetti.movieelements.User;
import it.massetti.sentiment.ScoreManager;
import it.massetti.sentiment.ScoreParameter;
import it.massetti.sentiment.SentenceAnalyzer;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class MovieHandler {
	
	List<Film> filmList;
	List<User> userList;
	int positive = 0;
	int negative = 0;
	int zero = 0;
	int catched = 0;
	int near = 0;
	int missed = 0;
	
	/**
	 * Builds a moviehandler from a Movielist. The userlist is inizialided
	 * @param movieList
	 * @throws IOException
	 */
	public MovieHandler (List<Film> movieList) throws IOException {
		filmList = movieList;
		userList = new ArrayList<User>();
	}
	
	/**
	 * Builds a moviehandler from a Movielist and a Userlist
	 * @param movieList
	 * @param idList
	 * @throws IOException
	 */
	public MovieHandler (List<Film> movieList, List<User> idList) throws IOException {
		filmList = movieList;
		userList = idList;
	}
	
	/**
	 * Generate sentiment Ratings from a List of RottenTomatoes Film and update the list
	 * @param filmList
	 * @return filmList updated
	 * @throws IOException
	 */
	public List<Film> ObtainRatings () throws IOException {
		SentenceAnalyzer analyzer = new SentenceAnalyzer();
		ScoreManager converter = new ScoreManager();
		PrintWriter fileout = new PrintWriter(new FileWriter("res/output.txt"));
		Iterator<Film> filmIterator = filmList.iterator();
		while(filmIterator.hasNext()) {
			Film currentFilm = filmIterator.next();
			System.out.println("Processing = " + currentFilm.getTitle());	
			fileout.println("Processing = " + currentFilm.getTitle());
			List<Review> reviews = currentFilm.getListreviews().getReviews();
			Iterator<Review> reviewIterator = reviews.iterator();
			while(reviewIterator.hasNext()) {
				Review currentReview = reviewIterator.next();
				Integer rating;
				Integer convertedScore;
				try {
					System.out.println("Rewiew = " + currentReview.getQuote());
					fileout.println("Rewiew = " + currentReview.getQuote());
					rating = analyzer.analyze(currentReview.getQuote());
					if (rating == null) {
						reviewIterator.remove();
					} else {
						currentReview.setSentimentRating(rating);
						converter.UpdateMaps(rating, "Given");
						System.out.println("Sentiment Rating = " + rating);		
						fileout.println("Sentiment Rating = " + rating);
						countRating(rating);
						String score = currentReview.getOriginal_score();
						if (score == null) {
							reviewIterator.remove();
						} else {
							fileout.println("Original = " + score);
							System.out.println("Original = " + score);
							convertedScore = converter.convertScore(score);
							if(convertedScore == null) {
								reviewIterator.remove();
							} else {
								currentReview.setConverted_score(convertedScore);
								int convertedRating = (int) (convertedScore);
								converter.UpdateMaps(convertedRating, "Retrieved");
								fileout.println("Converted = " + convertedScore);
								System.out.println("Converted = " + convertedScore);
								confrontScore(rating, convertedScore);
							}
						}
					}
				} catch (NullPointerException e) {
					e.printStackTrace();
					System.exit(-1);
				}
			}
		}
		converter.displayScores();
		System.out.println("Catched = " + catched);	
		System.out.println("Near = " + near);	
		System.out.println("Missed = " + missed);	
		fileout.println("Catched = " + catched);	
		fileout.println("Near = " + near);	
		fileout.println("Missed = " + missed);	
		fileout.close();
		return filmList;
	}
	
	/**
	 * Generate sentiment Ratings from a List of RottenTomatoes Film and update the list
	 * @param filmList
	 * @return filmList updated
	 * @throws IOException
	 */
	public List<Film> UpdateRatings () throws IOException {
		SentenceAnalyzer analyzer = new SentenceAnalyzer();
		ScoreManager converter = new ScoreManager();
		PrintWriter fileout = new PrintWriter(new FileWriter("res/output.txt"));
		Iterator<Film> filmIterator = filmList.iterator();
		while(filmIterator.hasNext()) {
			Film currentFilm = filmIterator.next();
			System.out.println("Processing = " + currentFilm.getTitle());	
			fileout.println("Processing = " + currentFilm.getTitle());
			List<Review> reviews = currentFilm.getListreviews().getReviews();
			Iterator<Review> reviewIterator = reviews.iterator();
			while(reviewIterator.hasNext()) {
				Review currentReview = reviewIterator.next();
				Integer rating;
				Integer convertedScore;
				System.out.println("Rewiew = " + currentReview.getQuote());
				fileout.println("Rewiew = " + currentReview.getQuote());
				rating = analyzer.analyze(currentReview.getQuote());
				currentReview.setSentimentRating(rating);
				converter.UpdateMaps(rating, "Given");
				System.out.println("Sentiment Rating = " + rating);		
				fileout.println("Sentiment Rating = " + rating);
				countRating(rating);
				convertedScore = (int) currentReview.getConverted_score();
				currentReview.setConverted_score(convertedScore);
				int convertedRating = (int) (convertedScore);
				converter.UpdateMaps(convertedRating, "Retrieved");
				fileout.println("Converted = " + convertedScore);
				System.out.println("Converted = " + convertedScore);
				confrontScore(rating, convertedScore);
			}
		}
		converter.displayScores();
		System.out.println("Catched = " + catched);	
		System.out.println("Near = " + near);	
		System.out.println("Missed = " + missed);	
		fileout.println("Catched = " + catched);	
		fileout.println("Near = " + near);	
		fileout.println("Missed = " + missed);	
		fileout.close();
		return filmList;
	}

	private void confrontScore(Integer rating, Integer convertedScore) {
		if(rating==convertedScore) {
			catched++;
		} else if (rating+convertedScore==0) {
			missed++;
		} else {
			near++;
		}
	}

	private void countRating(Integer rating) {
		switch (rating) {
		case -1:
			negative++;
			break;
		case 0:
			zero++;
			break;
		case 1:
			positive++;
			break;
		}
		
	}

	/**
	 * Generate Users id
	 * @return list of users
	 */
	public List<User> generateUserId() {
		int id = 1;
		int reviewsNotProcessed = 0;
		for (Film currentFilm : filmList) {
			System.out.println("Processing = " + currentFilm.getTitle());	
			List<Review> reviews = currentFilm.getListreviews().getReviews();
			for (Review currentReview : reviews) {
				String author = currentReview.getCritic();
				if (!author.equals("")) {
					System.out.println("User " + author + " retrieved");
					User critic = null;
					try {
						critic = getUser(author);
						System.out.println(author + " was found");
					} catch (NoUserException e) {
						critic = new User(author, id);
						id++;
						System.out.println(author + " not found");
						userList.add(critic);
					} finally {
						critic.addReview(currentReview);
						System.out.println("Adding review " + currentReview.getQuote());
					}				
				} else {
					System.out.println("Review not processed. Total = " + reviewsNotProcessed++);
				}
			}
		}
		return userList;
	}

	/**
	 * Determinate if an user exists
	 * @param author
	 * @return true if exists
	 * @throws NoUserException 
	 */
	private User getUser(String author) throws NoUserException {
		for (User current : userList) {
			if(current.getName().toLowerCase().equals(author.toLowerCase()))
				return current;
		}
		throw new NoUserException();
	}
	
	/**
	 * Show the user on console (debug method)
	 */
	public void ShowUsers() {
		int totalReviews = 0;
		for (User current : userList) {
			System.out.println("User = " + current.getName());
			System.out.println("Total Reviews = " + current.getReviews().size());
			System.out.println();
		}
		System.out.println("Total Users = " + userList.size());
		for (Film currentFilm : filmList) {
			totalReviews = totalReviews + currentFilm.getListreviews().getReviews().size();
		}
		System.out.println("Total Films = " + filmList.size());
		System.out.println("Total Reviews = " + totalReviews);
		double average = totalReviews/userList.size();
		System.out.println("Average Reviews per User = " + average);
	}
	
	/**
	 * Find an user id. Throws and Exception if not Found
	 * @param name
	 * @return the user id
	 * @throws UserNotFoundException
	 */
	public int findId(String name) throws UserNotFoundException {
		for (User current : userList) {
			if(current.getName().toLowerCase().equals(name.toLowerCase())) {
				return current.getId();
			}
		}
		throw new UserNotFoundException();
	}

	/**
	 * Generate the two Matrixes and Exports them
	 * @throws IOException
	 */
	public void generateMatrixes() throws IOException {
		FileManager parser = new FileManager();
		String realRatingMatrix = generateMatrix("real");
		parser.exportMatrix(realRatingMatrix, "real");
		String sentimentRatingMatrix = generateMatrix("sentiment");
		parser.exportMatrix(sentimentRatingMatrix, "sentiment");
	}

	/**
	 * Generate a matrix
	 * @param mode
	 * @return
	 */
	private String generateMatrix(String mode) {
		String matrix = new String();
		int userlist_size = userList.size();
		int filmlist_size = filmList.size();
		int values_size = 0;
		for (Film currentFilm : filmList) {
			System.out.println("Processing = " + currentFilm.getTitle());	
			List<Review> reviews = currentFilm.getListreviews().getReviews();
			for (Review currentReview : reviews) {
				try {
					switch (mode) {
					case "real" :
						int converted_score = (int) currentReview.getConverted_score();
						if(converted_score!=0) {
							int userId = findId(currentReview.getCritic());
							matrix = matrix.concat(userId + " " + currentFilm.getId() + " " + converted_score);
							matrix = matrix.concat("\n");
							values_size++;
							break;
						}
					case "sentiment" :	
						int sentiment_rating = currentReview.getSentimentRating();
						if(sentiment_rating!=0) {
							int userId = findId(currentReview.getCritic());
							matrix = matrix.concat(userId + " " + currentFilm.getId() + " " + sentiment_rating);
							matrix = matrix.concat("\n");
							values_size++;
							break;
						}
					}
				} catch (NullPointerException | UserNotFoundException e) {
					
				}
			}
		}
		matrix = (userlist_size + " " + filmlist_size + " " + values_size + "\n").concat(matrix);
		return matrix;
	}

	/**
	 * Generate the Training Matrixes
	 * @throws IOException 
	 */
	public void generateTrainingMatrixes() throws IOException {
		int userlist_size = userList.size();
		int filmlist_size = filmList.size();
		int values_training_size = 0;
		int values_test_size = 0;
		FileManager parser = new FileManager();
		Random randomGenerator = new Random();
		String realRatingMatrix = generateMatrix("real");
		String realRatingMatrixTest = new String();
		String realRatingMatrixTraining = new String();
		String sentimentRatingMatrix = generateMatrix("sentiment");
		String sentimentRatingMatrixTest = new String();
		String sentimentRatingMatrixTraining = new String();
		Scanner reader = new Scanner(realRatingMatrix);
		while (reader.hasNext()) {
			int randomInt = randomGenerator.nextInt(10);
			for(int i=0; i<10 && reader.hasNext(); i++) {
				if(i==randomInt) {
					realRatingMatrixTraining = realRatingMatrixTraining.concat(reader.nextLine() + "\n");
					values_training_size++;
				} else {
					realRatingMatrixTest = realRatingMatrixTest.concat(reader.nextLine() + "\n");
					values_test_size++;
				}
			}
		}	
		realRatingMatrixTraining = (userlist_size + " " + filmlist_size + " " + values_training_size + "\n").concat(realRatingMatrixTraining);
		realRatingMatrixTest = (userlist_size + " " + filmlist_size + " " + values_test_size + "\n").concat(realRatingMatrixTest);
		parser.exportMatrix(realRatingMatrixTest, "realTest");
		parser.exportMatrix(realRatingMatrixTraining, "realTraining");
		reader.close();
		Scanner readerSentiment = new Scanner(sentimentRatingMatrix);
		values_training_size = 0;
		values_test_size = 0;
		while (readerSentiment.hasNext()) {
			int randomInt = randomGenerator.nextInt(10);
			for(int i=0; i<10 && readerSentiment.hasNext(); i++) {
				if(i==randomInt) {
					sentimentRatingMatrixTraining = sentimentRatingMatrixTraining.concat(readerSentiment.nextLine() + "\n");
					values_training_size++;
				} else {
					sentimentRatingMatrixTest = sentimentRatingMatrixTest.concat(readerSentiment.nextLine() + "\n");
					values_test_size++;
				}
			}
		}
		sentimentRatingMatrixTraining = (userlist_size + " " + filmlist_size + " " + values_training_size + "\n").concat(sentimentRatingMatrixTraining);
		sentimentRatingMatrixTest = (userlist_size + " " + filmlist_size + " " + values_test_size + "\n").concat(sentimentRatingMatrixTest);
		parser.exportMatrix(sentimentRatingMatrixTest, "sentimentTest");
		parser.exportMatrix(sentimentRatingMatrixTraining, "sentimentTraining");
		readerSentiment.close();
	}

	public List<Film> RemoveDuplicates() {
		List<Film> newList = new ArrayList<Film>();
		for(Film current : filmList) {
			boolean doubleFilm = false;
			for(Film newCurrent : newList) {
				if(newCurrent.getId()==current.getId()) {
					doubleFilm = true;
					break;
				}
			}
			if(!doubleFilm) {
				newList.add(current);
			}
		}
		filmList = newList;
		return filmList;
	}

	public List<Film> ObtainParameters() throws IOException {
		SentenceAnalyzer analyzer = new SentenceAnalyzer();
		ScoreManager converter = new ScoreManager();
		PrintWriter fileout = new PrintWriter(new FileWriter("res/output.txt"));
		Iterator<Film> filmIterator = filmList.iterator();
		List<Score> scores = new ArrayList<Score>();
		while(filmIterator.hasNext()) {
			Film currentFilm = filmIterator.next();
			System.out.println("Processing = " + currentFilm.getTitle());	
			fileout.println("Processing = " + currentFilm.getTitle());
			List<Review> reviews = currentFilm.getListreviews().getReviews();
			Iterator<Review> reviewIterator = reviews.iterator();
			while(reviewIterator.hasNext()) {
				Review currentReview = reviewIterator.next();
				Score rating;
				Integer convertedScore;
				System.out.println("Rewiew = " + currentReview.getQuote());
				fileout.println("Rewiew = " + currentReview.getQuote());
				rating = analyzer.analyzeToObtainDetailedScores(currentReview.getQuote());
				convertedScore = (int) currentReview.getConverted_score();
				rating.setRating(convertedScore);
				rating.analyzeScore();
				scores.add(rating);
			}
		}
		obtainValues(scores);
		converter.displayScores();
		fileout.close();
		return filmList;
	}

	private void obtainValues(List<Score> scores) {
		double negative_scores=0;
		int negative_total=0;
		double neutral_negative_scores = 0;
		int neutral_negative_total = 0;
		double neutral_positive_scores = 0;
		int neutral_positive_total = 0;
		double positive_scores=0;
		int positive_total=0;
		double quotient_scores=0;
		int quotient_total=0;
		double neutral_quotient=0;
		int neutral_quotient_total=0;
		for (Score current : scores) {
			switch (current.getScoreType()) {
				case 0:
					if(current.getRating()==-1) {
						negative_scores = negative_scores + current.getNeg_score();
						negative_total++;
					} else if(current.getRating()==0) {
						neutral_negative_scores = neutral_negative_scores + current.getNeg_score();
						neutral_negative_total++;
					}
					break;
				case 1:
					if(current.getRating()==1) {
						positive_scores = positive_scores + current.getPos_score();
						positive_total++;
					} else if(current.getRating()==0) {
						neutral_positive_scores = neutral_positive_scores + current.getPos_score();
						neutral_positive_total++;
					}
					break;
				case 2:
					if(current.getRating()!=0) {
						quotient_scores = quotient_scores + current.getQuotient();
						quotient_total++;					
					} else {
						neutral_quotient = neutral_quotient + current.getQuotient();
						neutral_quotient_total++;
					}
					break;
			}
		}	
		double average_negative_scores = negative_scores/negative_total;
		double average_neutral_negative = neutral_negative_scores/neutral_negative_total;
		double average_positive_scores = positive_scores/positive_total;
		double average_neutral_positive = neutral_positive_scores/neutral_positive_total;
		double average_quotient_scores = quotient_scores/quotient_total;
		double average_neutral_quotient_scores = neutral_quotient/neutral_quotient_total;
		System.out.println("Total simple negatives:" + negative_total);
		System.out.println("Total neutral negatives:" + neutral_negative_total);
		System.out.println("Total simple positive:" + positive_total);
		System.out.println("Total neutral positive:" + neutral_positive_total);
		System.out.println("Total emotional scores:" + quotient_total);
		System.out.println("Total neutral quotient scores:" + neutral_quotient_total);
		System.out.println("Average simple negatives:" + average_negative_scores);
		System.out.println("Average neutral negatives:" + average_neutral_negative);
		System.out.println("Average simple positive:" + average_positive_scores);
		System.out.println("Average neutral positive:" + average_neutral_positive);
		System.out.println("Average emotional scores:" + average_quotient_scores);
		System.out.println("Average neutral quotient scores:" + average_neutral_quotient_scores);
		double firstScale = (negative_total*average_negative_scores + neutral_negative_total*average_neutral_negative)/(negative_total+neutral_negative_total);
		double secondScale = (average_positive_scores*positive_total+average_neutral_positive*neutral_positive_total)/(positive_total+neutral_positive_total);
		double thirdScale = (average_quotient_scores*quotient_total+average_neutral_quotient_scores*neutral_quotient_total)/(quotient_total+neutral_quotient_total);
		System.out.println("First Scale" + firstScale);
		System.out.println("Second Scale" + secondScale);
		System.out.println("Third Scale" + thirdScale);
	}

	public List<ScoreParameter> obtainParameters(List<ScoreParameter> paramListI, List<ScoreParameter> paramListJ, List<ScoreParameter> paramListK) throws IOException {
		int totalreview = 0;
		SentenceAnalyzer analyzer = new SentenceAnalyzer();
		Iterator<Film> filmIterator = filmList.iterator();
		while(filmIterator.hasNext()) {
			Film currentFilm = filmIterator.next();
			System.out.println("Processing = " + currentFilm.getTitle());	
			List<Review> reviews = currentFilm.getListreviews().getReviews();
			Iterator<Review> reviewIterator = reviews.iterator();
			while(reviewIterator.hasNext()) {
				totalreview++;
				Review currentReview = reviewIterator.next();
				Score rating;
				Integer sentimentScore;
				Integer realScore;
				System.out.println("Rewiew" + totalreview + " = " + currentReview.getQuote());
				rating = analyzer.analyzeToObtainDetailedScores(currentReview.getQuote());
				realScore = (int) currentReview.getConverted_score();
				rating.analyzeScore();
				if(rating.getScoreType()==0) {
					for (ScoreParameter current : paramListI) {
						sentimentScore = analyzer.normalize(rating, current.getNeutralNegative(), current.getNeutralPositive(), current.getQuotient());	
						current.updateAccuracy(sentimentScore, realScore);
					}	
				} else if (rating.getScoreType()==1) {
					for (ScoreParameter current : paramListJ) {
						sentimentScore = analyzer.normalize(rating, current.getNeutralNegative(), current.getNeutralPositive(), current.getQuotient());	
						current.updateAccuracy(sentimentScore, realScore);
					}	
				} else if (rating.getScoreType()==2) {
					for (ScoreParameter current : paramListK) {
						sentimentScore = analyzer.normalize(rating, current.getNeutralNegative(), current.getNeutralPositive(), current.getQuotient());	
						current.updateAccuracy(sentimentScore, realScore);
					}	
				}
		
			}
		}
		paramListI.addAll(paramListJ);
		paramListI.addAll(paramListK);
		return paramListI;
	}
}
