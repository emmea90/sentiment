package it.massetti.filehandler;

import it.massetti.movieelements.Film;

import java.io.IOException;
import java.util.List;

import opennlp.tools.util.InvalidFormatException;
public class SentimentUpdate {
	
	
	public static void main (String[] args) throws InvalidFormatException, IOException {
		FileManager parser = new FileManager();
		List<Film> dataList = parser.obtainUpdatedList();
		MovieHandler movieHandler = new MovieHandler(dataList);
		dataList = movieHandler.ObtainRatings();
		parser.saveList(dataList);
	}

}
