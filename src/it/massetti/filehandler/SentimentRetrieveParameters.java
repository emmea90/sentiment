package it.massetti.filehandler;

import it.massetti.movieelements.Film;
import it.massetti.sentiment.ScoreParameter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import opennlp.tools.util.InvalidFormatException;
public class SentimentRetrieveParameters {
	
	private FileManager parser = new FileManager();
	private List<Film> dataList;
	private FileWriter file;
	private int i;
	private int j;
	private int k;
	private double bestI;
	private double bestJ;
	private double bestK;
	private int accuracyI;
	private int accuracyJ;
	private int accuracyK;
	
	public void retrieveParameters() throws IOException {
		parser = new FileManager();
		dataList = parser.obtainUpdatedList();
		file = parser.openTresholdFile();
		List<ScoreParameter> paramListI = new ArrayList<ScoreParameter>();
		List<ScoreParameter> paramListJ = new ArrayList<ScoreParameter>();
		List<ScoreParameter> paramListK = new ArrayList<ScoreParameter>();
		List<ScoreParameter> paramList = new ArrayList<ScoreParameter>();
		MovieHandler movieHandler = new MovieHandler(dataList);
		for(i=0; i<100; i++) {
			float convertedI = i/100.0f;
			paramListI.add(new ScoreParameter(convertedI, 0, 0, 0));
		}
		for(j=0; j<100; j++) {
			float convertedJ = j/100.0f;
			paramListJ.add(new ScoreParameter(0, convertedJ, 0, 1));
		}
		for(k=50; k<100; k++) {
			float convertedK = k/100.0f;
			paramListK.add(new ScoreParameter(0, 0, convertedK, 2));
		}
		paramList = movieHandler.obtainParameters(paramListI, paramListJ, paramListK);
		for(ScoreParameter current : paramList) {
			file.write("Type" + current.getType() +": " + current.getScore() + "\t Accuracy"  + current.getAccuracy() + "\n");
			if(current.getType()==0) {
				if(current.getAccuracy()>accuracyI) {
					accuracyI=current.getAccuracy();
					bestI=current.getNeutralNegative();
				}
			}
			if(current.getType()==1) {
				if(current.getAccuracy()>accuracyJ) {
					accuracyJ=current.getAccuracy();
					bestJ=current.getNeutralNegative();
				}
			}
			if(current.getType()==2) {
				if(current.getAccuracy()>accuracyK) {
					accuracyK=current.getAccuracy();
					bestK=current.getNeutralNegative();
				}
			}
		}
		file.write("\nBEST\tType 1: " + bestI + "\tAccuracy: " + accuracyI);
		file.write("\nBEST\tType 2: " + bestJ + "\tAccuracy: " + accuracyJ);
		file.write("\nBEST\tType 3: " + bestK + "\tAccuracy: " + accuracyK);
		file.close();
	}
	
	public static void main (String[] args) throws InvalidFormatException, IOException {
		SentimentRetrieveParameters sentimentRetrieve = new SentimentRetrieveParameters();
		sentimentRetrieve.retrieveParameters();
	}

}
