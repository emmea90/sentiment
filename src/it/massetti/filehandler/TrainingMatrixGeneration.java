package it.massetti.filehandler;

import it.massetti.movieelements.Film;
import it.massetti.movieelements.User;

import java.io.IOException;
import java.util.List;

import opennlp.tools.util.InvalidFormatException;

public class TrainingMatrixGeneration {
	public static void main (String[] args) throws InvalidFormatException, IOException {
		FileManager parser = new FileManager();
		List<Film> movieList = parser.obtainUpdatedList();
		List<User> userList = parser.obtainUserList();
		MovieHandler movieHandler = new MovieHandler(movieList, userList);
		movieHandler.generateTrainingMatrixes();
	}
}
