package it.massetti.filehandler;

public class Score {

	private Double pos_score;
	private Double neg_score;
	private Double total;
	private Double quotient;
	private Integer rating;
	private Integer scoreType;
	
	public Score (Double pos_score, Double neg_score, Double total, Double quotient) {
		this.pos_score = pos_score;
		this.neg_score = neg_score;
		this.total = total;
		this.quotient = quotient;
	}

	public Double getPos_score() {
		return pos_score;
	}

	public void setPos_score(Double pos_score) {
		this.pos_score = pos_score;
	}

	public Double getNeg_score() {
		return neg_score;
	}

	public void setNeg_score(Double neg_score) {
		this.neg_score = neg_score;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getQuotient() {
		return quotient;
	}

	public void setQuotient(Double quotient) {
		this.quotient = quotient;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}
	
	public void analyzeScore () {
		if(pos_score == null || pos_score == 0) {
			setScoreType(0);
		} else if(neg_score == null || neg_score == 0) {
			setScoreType(1);
		} else {
			setScoreType(2);
		}
	}

	public Integer getScoreType() {
		return scoreType;
	}

	public void setScoreType(Integer scoreType) {
		this.scoreType = scoreType;
	}
}
