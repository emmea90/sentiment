package it.massetti.filehandler;

import it.massetti.movieelements.Film;

import java.io.IOException;
import java.util.List;

import opennlp.tools.util.InvalidFormatException;
public class SentimentGeneration {
	
	
	public static void main (String[] args) throws InvalidFormatException, IOException {
		FileManager parser = new FileManager();
		List<Film> dataList = parser.obtainList();
		MovieHandler movieHandler = new MovieHandler(dataList);
		dataList = movieHandler.RemoveDuplicates();
		dataList = movieHandler.ObtainRatings();
		parser.saveList(dataList);
	}

}
