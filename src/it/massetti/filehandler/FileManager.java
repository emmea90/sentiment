package it.massetti.filehandler;

import it.massetti.movieelements.Film;
import it.massetti.movieelements.User;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class FileManager {
	
	String pathToInput;
	String pathToFilms;
	String pathToUsers;
	String pathToRealRatingMatrix;
	String pathToSentimentRatingMatrix;
	String pathToRealRatingTrainingMatrix;
	String pathToSentimentRatingTrainingMatrix;
	String pathToRealRatingTestMatrix;
	String pathToSentimentRatingTestMatrix;
	String pathToTestTresholds;
	
	public FileManager () {
	    pathToInput = "res/FilmReviews.txt";
	    pathToFilms = "res/FilmReviewsUpdate.txt";
	    pathToUsers = "res/FilmReviewsUsers.txt";
	    pathToRealRatingMatrix = "res/RealRatingMatrix.txt";
	    pathToSentimentRatingMatrix = "res/SentimentRatingMatrix.txt";
	    pathToRealRatingTestMatrix = "res/RealRatingTestMatrix.txt";
	    pathToSentimentRatingTestMatrix = "res/SentimentRatingTestMatrix.txt";
	    pathToRealRatingTrainingMatrix = "res/RealRatingTrainingMatrix.txt";
	    pathToSentimentRatingTrainingMatrix = "res/SentimentRatingTrainingMatrix.txt";
	    pathToTestTresholds = "res/TresholdTests.txt";
	}
	
	/**
	 * Return the film list from the JsonFile
	 * @return list of Film
	 * @throws IOException
	 */
	public List<Film> obtainList () throws IOException {
		String fileData = new String(Files.readAllBytes(Paths.get(pathToInput)));
		Type collectionType = new TypeToken<Collection<Film>>(){}.getType();
		List<Film> filmList = new Gson().fromJson(fileData, collectionType);
		return filmList;
	}
	
	/**
	 * Returns the update film list from the updated JsonFile
	 * @return updated list
	 * @throws IOException
	 */
	public List<Film> obtainUpdatedList () throws IOException {
		String fileData = new String(Files.readAllBytes(Paths.get(pathToFilms)));
		Type collectionType = new TypeToken<Collection<Film>>(){}.getType();
		List<Film> filmList = new Gson().fromJson(fileData, collectionType);
		return filmList;
	}
	
	/**
	 * Returns the user List
	 * @return user list
	 * @throws IOException
	 */
	public List<User> obtainUserList () throws IOException {
		String fileData = new String(Files.readAllBytes(Paths.get(pathToUsers)));
		Type collectionType = new TypeToken<Collection<User>>(){}.getType();
		List<User> userList = new Gson().fromJson(fileData, collectionType);
		return userList;
	}
	
	/**
	 * Save the film list in the updated file
	 * @param films
	 * @throws IOException
	 */
	public void saveList(List<Film> films) throws IOException {
		FileWriter writer = new FileWriter(pathToFilms);
		Gson gson = new Gson();
		String json = gson.toJson(films);
		writer.write(json);
		writer.close();
	}
	
	/**
	 * Save the user list in the user file
	 * @param users
	 * @throws IOException
	 */
	public void saveUsers(List<User> users) throws IOException {
		FileWriter writer = new FileWriter(pathToUsers);
		Gson gson = new Gson();
		writer.write(gson.toJson(users));
		writer.close();
	}

	/**
	 * Export the matrix of the type given in its file
	 * @param matrix
	 * @param type
	 * @throws IOException
	 */
	public void exportMatrix(String matrix, String type) throws IOException {
		FileWriter writer = null;
		switch (type) {
		case "real" :
			writer = new FileWriter(pathToRealRatingMatrix);
			break;
		case "realTraining" :
			writer = new FileWriter(pathToRealRatingTrainingMatrix);
			break;
		case "realTest" :
			writer = new FileWriter(pathToRealRatingTestMatrix);
			break;
		case "sentiment" :	
			writer = new FileWriter(pathToSentimentRatingMatrix);
			break;
		case "sentimentTest" :	
			writer = new FileWriter(pathToSentimentRatingTestMatrix);
			break;
		case "sentimentTraining" :	
			writer = new FileWriter(pathToSentimentRatingTrainingMatrix);
			break;
		}
		writer.write(matrix);
		writer.close();
	}
	
	public FileWriter openTresholdFile () throws IOException {
		FileWriter writer = null;
		writer = new FileWriter(pathToTestTresholds);
		return writer;
	}
	
}