SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `sentimentdb` DEFAULT CHARACTER SET utf8 ;
USE `sentimentdb` ;

-- -----------------------------------------------------
-- Table `sentimentdb`.`films`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sentimentdb`.`films` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rotten_id` INT NULL DEFAULT NULL,
  `title` LONGTEXT NULL DEFAULT NULL,
  `year` INT NULL DEFAULT NULL,
  `mpaa_rating` VARCHAR(45) NULL DEFAULT NULL,
  `runtime` INT NULL DEFAULT NULL,
  `critics_consensus` TEXT NULL DEFAULT NULL,
  `synopsis` LONGTEXT NULL DEFAULT NULL,
  `poster` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sentimentdb`.`film_release_dates`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sentimentdb`.`film_release_dates` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `films_id` INT NOT NULL,
  `theater` VARCHAR(45) NULL DEFAULT NULL,
  `dvd` VARCHAR(45) NULL DEFAULT NULL,
  INDEX `fk_release_dates_films1_idx` (`films_id` ASC),
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sentimentdb`.`film_ratings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sentimentdb`.`film_ratings` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `films_id` INT NOT NULL,
  `critics_rating` VARCHAR(45) NULL DEFAULT NULL,
  `critics_score` INT NULL DEFAULT NULL,
  `audience_rating` VARCHAR(45) NULL DEFAULT NULL,
  `audience_score` INT NULL DEFAULT NULL,
  INDEX `fk_ratings_films_idx` (`films_id` ASC),
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sentimentdb`.`film_abridged_casts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sentimentdb`.`film_abridged_casts` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `films_id` INT NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  INDEX `fk_abriged_cast_films1_idx` (`films_id` ASC),
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sentimentdb`.`film_characters`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sentimentdb`.`film_characters` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `abriged_cast_id` INT NOT NULL,
  `charachter` TEXT NULL DEFAULT NULL,
  INDEX `fk_charachters_abriged_cast1_idx` (`abriged_cast_id` ASC),
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sentimentdb`.`film_alternate_ids`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sentimentdb`.`film_alternate_ids` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `films_id` INT NOT NULL,
  `imdb` INT NULL DEFAULT NULL,
  INDEX `fk_alternate_ids_films1_idx` (`films_id` ASC),
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sentimentdb`.`film_links`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sentimentdb`.`film_links` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `films_id` INT NOT NULL,
  `self` TEXT NULL DEFAULT NULL,
  `alternate` TEXT NULL DEFAULT NULL,
  `cast` TEXT NULL DEFAULT NULL,
  `clips` TEXT NULL DEFAULT NULL,
  `reviews` TEXT NULL DEFAULT NULL,
  `similar` TEXT NULL DEFAULT NULL,
  INDEX `fk_links_films1_idx` (`films_id` ASC),
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sentimentdb`.`review_users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sentimentdb`.`review_users` (
  `id` INT NULL DEFAULT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sentimentdb`.`reviews`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sentimentdb`.`reviews` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `films_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  `critic` VARCHAR(45) NULL DEFAULT NULL,
  `date` VARCHAR(45) NULL DEFAULT NULL,
  `freshness` VARCHAR(45) NULL DEFAULT NULL,
  `publication` VARCHAR(45) NULL DEFAULT NULL,
  `quote` LONGTEXT NULL DEFAULT NULL,
  `original_score` TEXT NULL DEFAULT NULL,
  `converted_score` DECIMAL NULL DEFAULT NULL,
  `sentiment_rating` INT NULL DEFAULT NULL,
  INDEX `fk_reviews_films1_idx` (`films_id` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_reviews_user1_idx` (`user_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sentimentdb`.`review_links`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `sentimentdb`.`review_links` (
  `reviews_id` INT NOT NULL AUTO_INCREMENT,
  `reviewlink` TEXT NULL DEFAULT NULL,
  INDEX `fk_reviewlinks_reviews1_idx` (`reviews_id` ASC),
  PRIMARY KEY (`reviews_id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
